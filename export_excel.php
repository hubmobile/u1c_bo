<?php

include("includes/ezsql.php");

// On selectionne toutes les pathos
$fiches = $ezdb->get_results('SELECT * FROM pathologies');

?>

    <table border="1">
        <tr>
            <th>id_pathologies</th>
            <th>statut_pathologies</th>
            <th>cible_pathologies</th>
            <th>titre_pathologies</th>
            <th>specialite_pathologies</th>
            <th>r_pathologies</th>
            <th>auteur_pathologies</th>
        </tr>

<?php

$no = 1;

foreach($fiches as $fiche)
{
    echo '
        <tr>
            <td>'.$fiche->id_pathologies.'</td>
            <td>'.$fiche->statut_pathologies.'</td>
            <td>'.$fiche->cible_pathologies.'</td>
            <td>'.$fiche->titre_pathologies.'</td>
            <td>'.$fiche->specialite_pathologies.'</td>
            <td>'.$fiche->r_pathologies.'</td>
            <td>'.$fiche->auteur_pathologies.'</td>
        </tr>
    ';
    $no++;
}

// The function header by sending raw excel
header("Content-type: application/vnd-ms-excel");
// Defines the name of the export file "codelution-export.xls"
header("Content-Disposition: attachment; filename=codelution-export.xls");

?>