/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
  {

  config.language = 'fr';
  config.uiColor = '#D8D8D8';
  //config.stylesCombo_stylesSet = 'my_styles';
  //ligne de code si les styles sont définies dans la page
  //config.stylesCombo_stylesSet = 'my_styles:./mes_styles.js';
  //config.stylesCombo_stylesSet = 'my_styles:http://www.example.com/styles.js';
  config.toolbar = 'ToolbarUrgences';
  config.extraPlugins = 'mediaOnline';
config.toolbar_ToolbarUrgences =
  [
    ['Preview','Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','Format'],
    '/',
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['mediaOnline_media','mediaOnline_linkMedia','Link','Table','HorizontalRule','SpecialChar'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','Source']
  ];
  };
/*config.toolbar_Full =
[
    ['Source','-','Save','NewPage','Preview','-','Templates'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
    ['BidiLtr', 'BidiRtl'],
    '/',
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['Link','Unlink','Anchor'],
    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
    '/',
    ['Styles','Format','Font','FontSize'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','About']
];
*/

