var mediaWidth = 640;
var mediaHeight = 490;	

CKEDITOR.plugins.add('mediaOnline',{
  requires: ['iframedialog'],
  init:function(editor){
	  CKEDITOR.dialog.add( 'mediaOnline_mediaDialog', function ()
			  {
		  return {
			  title : 'Insérer un média',
			  minWidth : mediaWidth,
			  minHeight : mediaHeight,
			  contents :
				  [
				   {
					   id : 'iframeMedia',
					   label : 'Ajouter un média',
					   expand : true,
					   elements :
						   [
						    {
						    	type : 'iframe',
						    	src : 'js/ckeditor/plugins/mediaOnline/dialogs/frameMedia.php',
						    	width : mediaWidth,
						    	height : CKEDITOR.env.ie ? mediaHeight - 10 : mediaHeight,
						    			onContentLoad : function(){}
						    }
						    ]
				   }
				   ]
		  };
			  });
	  CKEDITOR.dialog.add( 'mediaOnline_linkMediaDialog', function ()
			  {
		  
		  		return {
			      title : 'Insérer un lien',
			      minWidth : mediaWidth,
			      minHeight : mediaHeight,
			      contents :
			        [
			          {
			            id : 'iframeLink',
			            label : 'Ajouter un lien',
			            elements :
			              [
			                {
			                  type : 'iframe',
			                  src : 'js/ckeditor/plugins/mediaOnline/dialogs/frameLink.php',
			                  width : mediaWidth,
			                  height : CKEDITOR.env.ie ? mediaHeight - 10 : mediaHeight,
			                  onContentLoad : function(){}
			                }
			              ]
			          }
			        ]
			    };
			  });
	  
    editor.addCommand('mediaOnline_media', {exec:mediaOnline_runMedia});
    editor.addCommand('mediaOnline_linkMedia', {exec:mediaOnline_runLinkMedia});
    editor.ui.addButton('mediaOnline_media',{ label:'Ajouter un média', command:'mediaOnline_media',icon:'plugins/mediaOnline/images/media.gif' });
    editor.ui.addButton('mediaOnline_linkMedia',{ label:'Ajouter un lien', command:'mediaOnline_linkMedia',icon:'plugins/mediaOnline/images/link.gif' });
    
  }
});

function mediaOnline_runMedia(editor)
{
	// run when custom button is clicked
	
	editor.openDialog('mediaOnline_mediaDialog');
}
function mediaOnline_runLinkMedia(editor)
{
  // run when custom button is clicked
	editor.openDialog('mediaOnline_linkMediaDialog');
}