<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<?php
require_once dirname(__FILE__).'/../config.php';
require_once dirname(__FILE__).'/../../../../../includes/access.class.php';

$db = new flexibleAccess();

$from = isset($_REQUEST['from']) ? $_REQUEST['from'] : "" ;

switch($from){
	case 'medias':
		if(isset($_REQUEST['datas'])){
			$search = $_REQUEST['datas'] ;
			$sql = "SELECT * FROM ".$table1." ";
			if($search!="*"){
				$searchCol = $table1_searchColonne ;
				$operateur = " LIKE '%".$search."%'";
				if(strpos($search,"#")!==false){
					$searchCol = $table1_champsId;
					$search = str_replace('#','',str_replace('MEDIA_','',$search));
					$operateur = "= '".$search."'";
				}
				
				$sql = "SELECT * FROM ".$table1." WHERE ".$searchCol.$operateur;
			}	 
			
			$res = $db->query($sql);
			?>
			<script type="text/javascript">
					$(document).ready(function(){
				   // Your code here
				   idMedia = "" ;
					$("#content tr").hover(function(){
						   $(this).addClass("hover");
					 },function(){
					   $(this).removeClass("hover");
					 });
					$("#content tr").live('click',function(){
						idMedia = $(this).attr('id');
						$(".selected").removeClass("selected");
						$(this).addClass("selected");
						$("#"+butonOk, window.parent.document).css('display','');
						//alert(idMedia);
					});
					 
			});
			 </script>
			<table width="99%" cellspacing="0" cellpadding="0">
				<?php 
				$i=0;
				while ($media = mysqli_fetch_assoc($res)) {
					$class = $i%2==0 ? "class='paire'" : "" ;
					if(file_exists('../../../../../'.$image_dir.$media[$table1_champsImage])!==false){
					  echo '
					  <tr '.$class.'   id="#MEDIA_'.$media[$table1_champsId].'#">
					  <td width="120" align="center"><img src="../../../../../medias/vignettes/'.$media[$table1_champsImage].'" width="100px" /> </td>
					  <td>
					  	<span class="titre">'.utf8_decode($media[$table1_champs1]).'</span>
					  	<p>'.utf8_decode($media[$table1_champs2]).'</p>
					  </td>
					  </tr>
					  ';
					  $i++;
						}
					}
				?>
				</table>
			<?php 
			
		}
		break;
	case 'links' :
		if(isset($_REQUEST['type'])){
			$link = $links[$_REQUEST['type']];
			?>
			<script type="text/javascript">
			jQuery(document).ready(function(){
			//	CKEDITOR = window.parent.CKEDITOR;
			$('#search').bind('keyup', function(){
				var content = $(this).val();
				var params ="*";
				//alert(content);
				if(content!=""){
					params = content;
				}
				urlType = "<?php echo $link['url']; ?>";
				
				//alert($("#"+butonOk).attr('href'));
				$.ajax({
					  type: "POST",
					  url: 'ajaxFunctions.php?from=searchLinks',
					  data: "type=<?php echo $_REQUEST['type'];?>&datas="+params,
					  success: function(data) {
					    $('#subcontent').html(data);
					  //  alert('Load was performed.');
					  }
				});

			   
		   });
			$('#search').keyup();
			});
			</script>
			<table  width="100%" cellpadding="0" cellspacing="0"><tr><td width="150px">Rechercher :</td><td><input name="search" id="search" style="width:100%;"/></td></tr></table>
			<hr />
		
			<div id="subcontent" style="height:320px;overflow:auto;"></div>
			
			<hr />
			<table  width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="150px" rowspan="2">Class :</td>
				<td><input name="class" id="classText" style="width:50%;"/></td>
			</tr>
			<tr>
			<td> ou <select name="classSelect" id="classSelect"><option value="">-- Choisir --</option>
			<?php 
			$classes = $link['classes'];
			if(sizeOf($classes)>0){
				foreach ($classes  as $class){
					echo "<option value='".$class."'>".$class."</option>";
				}
			}
			?>
			</select>
			</td>
			</tr>
			</table>
			<?php 
		}
		break;
	case 'searchLinks' :
		if(isset($_REQUEST['type'])){
			$link = $links[$_REQUEST['type']];
			$table=$link['table'];
			$titleTab=$link['titleTab'];
			$champsId=$link['champsId'];
			$champs1=$link['champs1'];
			$champs2=(isset($link['champs2']) AND $link['champs2']!="") ? $link['champs2'] : "";
			$searchColonne=$link['searchColonne'];
			$url=$link['url'];
			
			$search = $_REQUEST['datas'] ;
			$sql = "SELECT * FROM ".$table." ";
			if($search!="*"){
				$searchCol = $searchColonne ;
				$operateur = " LIKE '%".$search."%'";
				if(strpos($search,"#")!==false){
					$searchCol = $champsId;
					$search = str_replace('#','',str_replace('MEDIA_','',$search));
					$operateur = "= '".$search."'";
				}
				
				$sql = "SELECT * FROM ".$table." WHERE ".$searchCol.$operateur;
			}	 
			
			$res = $db->query($sql);
			?>
			<script type="text/javascript">
			jQuery(document).ready(function(){
				//CKEDITOR = window.parent.CKEDITOR;
				   // Your code here
				   idLink = "" ;
				   //myClassName = "" ;
					
					$("#subcontent .row").hover(function(){
						   $(this).addClass("hover");
					 },function(){
					   $(this).removeClass("hover");
					 });
					$("#subcontent .row").live('click',function(){
						
						idLink = $(this).attr('id');
							
					//   alert(myClassName);
						$(".selected").removeClass("selected");
						$(this).addClass("selected");
						$("#"+butonOk, window.parent.document).css('display','');
						//alert(idMedia);
					});
					 
				});
			 </script>
			<table width="99%" cellspacing="0" cellpadding="0">
				<?php 
				$i=0;
				while ($row = mysqli_fetch_assoc($res)) {
					$class = $i%2==0 ? 'paire' : "" ;
					  echo '
					  <div class="row '.$class.'" id="#LINK_'.$row[$champsId].'#" style="font-size : 11 px ;">
					  	<span class="titre">'.($row[$champsId]." - ".utf8_decode($row[$champs1])).'</span>';

					  echo $champs2!="" ? ' - <i>'.utf8_decode($row[$champs2]).'</i>' : '';
					  echo '</div>';
					  $i++;
				}
				?>
				</table>
			<?php 
		}
		break;	
	default :
		break;		
	
}



?>