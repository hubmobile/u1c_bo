<?php
require_once dirname(__FILE__).'/../config.php';
require_once dirname(__FILE__).'/../../../../../includes/access.class.php';

$db = new flexibleAccess();


?>
<style type="text/css">

span.titre{
font-size : 14 px ;
font-weight:bold;
}
p{
font-size : 12 px ;
font-style : italic;
}
tr.paire{
background : #F1F1F1;
}
tr.hover{
background : #FFF;
border : 1px solid black;
cursor:pointer;
}
tr.hover td{
border : 1px solid black;
}
tr.selected{
background:#B4E629;
border : 1px solid black;
}
</style>
<script src="../../../../jquery-1.4.4.min.js" ></script>

<script type="text/javascript">
var CKEDITOR = window.parent.CKEDITOR;
var idMedia ;
CKEDITOR.dialog.getCurrent().removeListener("ok", okListener);
var okListener = function(event) {
    this._.editor.insertHtml(""+idMedia);
    CKEDITOR.dialog.getCurrent().removeListener("ok", okListener);
  };
  var cancelListener = function(event) {
	   idMedia = "" ;
	    CKEDITOR.dialog.getCurrent().removeListener("ok", okListener);
	  };  
  CKEDITOR.dialog.getCurrent().on("ok", okListener);
  CKEDITOR.dialog.getCurrent().on("cancel", cancelListener);
  //CKEDITOR.dialog.getCurrent().disableButton();
  var butonOk = CKEDITOR.dialog.getCurrent().getButton("ok").domId;
 
  
$(document).ready(function(){   
	$("#"+butonOk, window.parent.document).css('display','none');
	idMedia ="";
	$('#search').bind('keyup', function(){
			var content = $(this).val();
			var params ="*";
			//alert(content);
			if(content!=""){
				params = content;
			}
			//alert($("#"+butonOk).attr('href'));
			$.ajax({
				  type: "POST",
				  url: 'ajaxFunctions.php?from=medias',
				  data: "datas="+params,
				  success: function(data) {
				    $('#content').html(data);
				  //  alert('Load was performed.');
				  }
			});

		   
	   });
	$('#search').keyup();  
 });
</script>

<table  width="100%"><tr><td width="150px">Rechercher :</td><td><input name="search" id="search" style="width:100%;"/></td></tr></table>
<hr />
<div style="height:400px;overflow:auto;" id="content">

</div>