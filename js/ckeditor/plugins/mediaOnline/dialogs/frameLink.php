<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
require_once dirname(__FILE__).'/../config.php';
require_once dirname(__FILE__).'/../../../../../includes/access.class.php';

$db = new flexibleAccess();


?>
<style type="text/css">

*{
font-size : 11 px ;
}

div.paire{
background : #CCC;
}
div.hover{
background : #FFF;
border : 1px solid black;
cursor:pointer;
}
div.selected{
background:#B4E629;
border : 1px solid black;
}
</style>
<link rel="stylesheet" href="../../../../jquery-ui.css" type="text/css" media="all" /> 
<script src="../../../../jquery-1.4.4.min.js" ></script>
<script src="../../../../jquery-ui.min.js" type="text/javascript"></script> 
<script type="text/javascript">
/* if(CKEDITOR.currentInstance){

	  	myEditor =CKEDITOR.currentInstance ;
	  	CKEDITOR.dialog.getCurrent().removeListener("ok", okListener);
	  	var mySelection = myEditor.getSelection();
	  	if (CKEDITOR.env.ie) {
	  	    mySelection.unlock(true);
	  	    selectedText = mySelection.getNative().createRange().text;
	  	} else {
	  	    selectedText = mySelection.getNative();
	  	}
	  }*/
var CKEDITOR = "" ; 
var idLink ;
var urlType ;
var myClassName ="";
var myEditor = "";
var selectedText ="";
var butonOk = "";
	$(document).ready(function() { 
	  

	  CKEDITOR = window.parent.CKEDITOR;
	
	  var okListener = function(event) {
	  	idLink=idLink.replace("#","");
	  	idLink=idLink.replace("LINK_","");
	  	idLink=idLink.replace("#","");
	  	urlType =urlType.replace("#ID#",idLink);
	  	
	  	if($("#classText").val()!=""){
	  		myClassName = $("#classText").val();
	  	}else if($("#classSelect option:selected").val()!=""){
	  		myClassName = $("#classSelect option:selected").val();
	  	}	
	  	var laClasse = "" ;
	  	if(myClassName!=""){
	  		laClasse = " class='"+myClassName+"'";
	  	}
	  	var mySelection = this._.editor.getSelection();
	  	if (CKEDITOR.env.ie) {
	  	    mySelection.unlock(true);
	  	    selectedText = mySelection.getNative().createRange().text;
	  	} else {
	  	    selectedText = mySelection.getNative();
	  	}
	  	if(selectedText!=""){
	  		var link = "<a href='"+urlType+"' "+laClasse+">"+selectedText+"</a>";
  	     	this._.editor.insertHtml(""+link);
	  	}else{
		  	alert("Veuillez sélectionner du texte");
	  	}
	      CKEDITOR.dialog.getCurrent().removeListener("ok", okListener);
	    //  CKEDITOR=myEditor=undefined;
	    };
	    var cancelListener = function(event) {
	  	  CKEDITOR.dialog.getCurrent().removeListener("ok", okListener);
	  	 // CKEDITOR=myEditor=undefined;
	    };  
	    CKEDITOR.dialog.getCurrent().on("ok", okListener);
	    CKEDITOR.dialog.getCurrent().on("cancel", cancelListener);
	    
	    butonOk = CKEDITOR.dialog.getCurrent().getButton("ok").domId;

	  
	$("#"+butonOk, window.parent.document).css('display','none');
	idLink ="";
	urlType ="";
	$( "#tabs" ).tabs({ajaxOptions: { cache: false } });
	
 });
</script>

<div style="height:460px;overflow:hidden;" id="content">
 
<div id="tabs" > 
	<ul> 
	<?php 
	$i=0;
	foreach($links as $link){?>
		<li><a title="tabsContent" href="ajaxFunctions.php?from=links&type=<?php echo $i;?>"><?php echo $link['titleTab'] ?></a></li>
	<?php 
	$i++;
	}?>	
	</ul>
	<div style="height:455px;overflow:hidden;" id="tabsContent">
	</div> 
</div> 
</div>