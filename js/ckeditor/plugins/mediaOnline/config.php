<?php
$table1 = "medias";
$table1_searchColonne = "nom_medias";
$table1_champs1 = "nom_medias";
$table1_champs2 = "legende_medias";
$table1_champsImage = "vignette_medias";
$table1_champsId = "id_medias";
$image_dir="medias/vignettes/";
/*#PROTOCOLE#
 * - pathologies://
 * - techniques://
 * - scores://
 */

$links = array(
	0 =>array(
		"table"=>"pathologies",
		"titleTab"=>"Pathologies",
		"champsId"=>"id_pathologies",
		"champs1"=>"titre_pathologies",
		"champs2"=>"",
		"searchColonne"=>"titre_pathologies",
		"url"=>"pathologie://pathologies_#ID#.html",
		"classes"=>array("pathologiesLink")
		),
	1 =>array(
		"table"=>"techniques",
		"titleTab"=>"Techniques",
		"champsId"=>"id_techniques",
		"champs1"=>"titre_techniques",
		"champs2"=>"",
		"searchColonne"=>"titre_techniques",
		"url"=>"technique://techniques_#ID#.html",
		"classes"=>array("techniquesLink")	),
	2 =>array(
		"table"=>"scores",
		"titleTab"=>"Scores",
		"champsId"=>"id_scores",
		"champs1"=>"nom_scores",
		"champs2"=>"legende_scores",
		"searchColonne"=>"nom_scores",
		"url"=>"score://scores_#ID#.html",
		"classes"=>array("scoresLink")	)
	);