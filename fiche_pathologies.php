<?php

    if(isset($_GET['id']))
    {
        include("includes/config.php");
        include("includes/fonctions.php");
        include("includes/ezsql.php");

        // On récupère les informations de la fiche
        $idFiche = $_GET['id'];
        $fiche = $ezdb->get_row("SELECT * FROM pathologies WHERE id_pathologies='".$idFiche."'");
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/view.css" rel="stylesheet" type="text/css" />
<title><?php echo $fiche->titre_pathologies; ?></title></head>
<body>
    <div id="content">
    <div class="entete"><b><?php echo id2champ('cibles','label_cibles','id_cibles',$fiche->cible_pathologies); ?></b>

    <br />
    Sp&eacute;cialit&eacute; : <?php
    $specialites = explode(",", $fiche->specialite_pathologies);
    foreach($specialites as $specialite){
        echo id2champ('specialites','label_specialites','id_specialites',$specialite)." / ";
    } ?>
</div>
<div class="contenu">

<?php
    if($fiche->pi_pathologies != "")
        echo '<h1><a name="paragPI"></a>Points importants</h1>'.$fiche->pi_pathologies;
?>

<?php
    if($fiche->c_pathologies != "")
        echo '<h1><a name="paragC"></a>Présentation clinique / CIMU</h1>'.$fiche->c_pathologies;
?>

<?php
    if($fiche->sp_pathologies != "")
        echo '<h1><a name="paragSP"></a>Signes paracliniques</h1>'.$fiche->sp_pathologies;
?>

<?php
    if($fiche->de_pathologies != "")
        echo '<h1><a name="paragDE"></a>Diagnostic étiologique</h1>'.$fiche->de_pathologies;
?>

<?php
    if($fiche->dd_pathologies != "")
        echo '<h1><a name="paragDD"></a>Diagnostic différentiel</h1>'.$fiche->dd_pathologies;
?>

<?php
    if($fiche->t_pathologies != "")
        echo '<h1><a name="paragPE"></a>Traitement</h1>'.$fiche->t_pathologies;
?>

<?php
    if($fiche->s_pathologies!="")
        echo '<h1><a name="paragS"></a>Surveillance</h1>'.$fiche->s_pathologies;
?>

<?php
    if($fiche->d_pathologies!="")
        echo '<h1><a name="paragC"></a>Devenir / orientation</h1>'.$fiche->d_pathologies;
?>

<?php
    if($fiche->md_pathologies!="")
        echo '<h1><a name="paragMD"></a>Mécanisme / description</h1>'.$fiche->md_pathologies;
?>

<?php
    if($fiche->a_pathologies!="")
        echo '<h1><a name="paragA"></a>Algorithme</h1>'.$fiche->a_pathologies;
?>

<?php
    if($fiche->b_pathologies!="")
        echo '<h1><a name="paragB"></a>Bibliographie</h1>'.$fiche->b_pathologies;
?>

<p style="font-weight: bold;">Auteur : <?php echo $fiche->auteur_pathologies;?></p>
</div>
</div>
</body></html>