<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");
    include("includes/fonctions.php");

    function recurse_copy($src, $dst) 
    {
        $dir = opendir($src);
        
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) 
        {
            if (( $file != '.' ) && ( $file != '..' )) 
            {
                if ( is_dir($src . '/' . $file) ) 
                {
                    recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else 
                {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        
        closedir($dir);
    }

    function forceFilePutContents($filepath, $message)
    {
        try 
        {
            $isInFolder = preg_match("/^(.*)\/([^\/]+)$/", $filepath, $filepathMatches);
            
            if($isInFolder) 
            {
                $folderName = $filepathMatches[1];
                $fileName = $filepathMatches[2];
                
                if (!is_dir($folderName))
                    mkdir($folderName, 0777, true);
            }
            file_put_contents($filepath, $message);
        } 
        catch (Exception $e) 
        {
            echo "ERR: error writing '$message' to '$filepath', ". $e->getMessage();
        }
    }

    function delete_directory($dirname) 
    {
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        
        if (!isset($dir_handle))
            return false;

        if (!empty($dir_handle))
            return false;
        
        while($file = readdir($dir_handle)) 
        {
            if ($file != "." && $file != "..") 
            {
                if (!is_dir($dirname."/".$file))
                    unlink($dirname."/".$file);
                else
                    delete_directory($dirname.'/'.$file);
            }
        }
     
        closedir($dir_handle);
     
        rmdir($dirname);
        return true;
    }
?>

<div id="middle">
    <div id="left-column">
        <h3>Exportation JSON</h3>
        <ul class="nav">
            <li>Cette page permet de générer le fichier ZIP global</li>
        </ul>
    </div>
    
    <div id="center-column">
        <div class="table">

            <div class="top-bar">
                <h1>Exportation ZIP</h1>
                <p style="padding: 10px;"><b>Attention :</b> cette rubrique est réservée aux administrateurs</p>
            </div>

            <div style="clear:both;">&nbsp;</div>
            
            <div class="logs_export">
            
                <?php
                    delete_directory('ressources/1/');

                    if (file_exists('ressources/ressources.zip'))
                        unlink('ressources/ressources.zip');

                    echo "<h2>Logs exportation (le lien du fichier sera disponible en bas de page à la fin du script)</h2></br></br>";
                
                    $specialites = array(
                        '1' => 'A',
                        '2' => 'B',
                        '3' => 'C',
                        '4' => 'D',
                        '5' => 'E',
                        '6' => 'F',
                        '7' => 'G',
                        '8' => 'H',
                        '9' => 'I',
                        '10' => 'J',
                        '11' => 'K',
                        '12' => 'L',
                        '13' => 'M',
                        '14' => 'N',
                        '15' => 'O',
                        '16' => 'P',
                        '17' => 'Q',
                        '18' => 'R',
                        '19' => 'S',
                        '20' => 'T',
                        '21' => 'U',
                        '22' => 'V',
                        '23' => 'W',
                        '24' => 'X',
                        '25' => 'Y',
                        '26' => 'Z'
                    );

                    // Init array global
                    $arrayJSON = array();

                    // PATHOLOGIES
                    echo "<h2>Traitement des <b>pathologies</b></h2>";
                
                    // Init array pathologies
                    $arrayPathologies = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM pathologies WHERE titre_pathologies REGEXP "^'.$value.'" ORDER BY titre_pathologies ASC');

                        if(isset($items))
                        {
                            echo "<p>Traitement des fiches de la <b>lettre $value</b></p>";

                            // Init array de fiches
                            $fiches = array();

                            foreach($items as $item)
                            {
                                //$trans = array("&#39;" => "'","&OElig;" => "oe","&oelig;" => "oe");
                                
                                echo "<li>Fiche n°$item->id_pathologies : $item->titre_pathologies</li>";

                                // Création array chapters et vérification des chapitres
                                $chapters = array();

                                if($item->pi_pathologies!="") array_push($chapters, "PI");
                                if($item->c_pathologies!="") array_push($chapters, "C");
                                if($item->sp_pathologies!="") array_push($chapters, "SP");
                                if($item->de_pathologies!="") array_push($chapters, "DE");
                                if($item->dd_pathologies!="") array_push($chapters, "DD");
                                if($item->t_pathologies!="") array_push($chapters, "T");
                                if($item->s_pathologies!="") array_push($chapters, "S");
                                if($item->d_pathologies!="") array_push($chapters, "D");
                                if($item->md_pathologies!="") array_push($chapters, "MD");
                                if($item->a_pathologies!="") array_push($chapters, "A");
                                if($item->b_pathologies!="") array_push($chapters, "B");

                                // Initialisation array fiche
                                $fiche = array(
                                    'title' => $item->titre_pathologies,
                                    'file' => "pathologies_".$item->id_pathologies.".html",
                                    'coverage' => $item->cible_pathologies,
                                    'keywords' => $item->r_pathologies,
                                    'authors' => $item->auteur_pathologies,
                                    'chapters' => $chapters,
                                    'speciality' => $item->specialite_pathologies,
                                    'action' => "ADD"
                                );

                                // On boucle sur les médias de cette fiche
                                $mediasAll = $ezdb->get_results('SELECT * FROM medias WHERE pathologie_medias REGEXP "(^'.$item->id_pathologies.'$)|(^'.$item->id_pathologies.',)|(,'.$item->id_pathologies.',)|(,'.$item->id_pathologies.'$)" ORDER BY ordre_medias ASC');

                                if(isset($mediasAll))
                                {
                                    // Création array medias
                                    $medias = array();

                                    foreach($mediasAll as $media)
                                    {
                                        echo "<ul><li>Media ".$media->id_medias." >> ".$media->nom_medias."</li></ul>";

                                        $mediaArray = array(
                                            'title' => $media->nom_medias,
                                            'subtitle' => $media->legende_medias,
                                            'type' => $media->type_medias,
                                            'thumb' => $media->vignette_medias,
                                            'file' => $media->fichier_medias
                                        );

                                        array_push($medias, $mediaArray);
                                    }

                                    $fiche['medias'] = $medias;
                                }

                                array_push($fiches, $fiche);
                            }

                            $arrayPathologies[$value] = $fiches;

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de fiche pour la <b>lettre $value</b></p>";
                        }
                    }

                    $arrayJSON["pathologies"] = $arrayPathologies;

                
                    // TECHNIQUES
                    echo "<h2>Traitement des <b>techniques</b></h2>";
                    
                    // Init array techniques
                    $arrayTechniques = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM techniques WHERE titre_techniques REGEXP "^'.$value.'" ORDER BY titre_techniques ASC');

                        if(isset($items))
                        {
                            echo "<p>Traitement des fiches de la <b>lettre $value</b></p>";

                            // Init array de fiches
                            $fiches = array();

                            foreach($items as $item)
                            {
                                echo "<li>Fiche n°$item->id_techniques : $item->titre_techniques</li>";
                                
                                // Création array chapters et vérification des chapitres
                                $chapters = array();
                                
                                if($item->pi_techniques!="") array_push($chapters, "PI");
                                if($item->i_techniques!="") array_push($chapters, "I");;
                                if($item->ci_techniques!="") array_push($chapters, "CI");
                                if($item->pm_techniques!="") array_push($chapters, "PM");
                                if($item->dt_techniques!="") array_push($chapters, "DT");
                                if($item->pe_techniques!="") array_push($chapters, "PE");
                                if($item->p_techniques!="") array_push($chapters, "P");
                                if($item->c_techniques!="") array_push($chapters, "C");
                                if($item->s_techniques!="") array_push($chapters, "S");
                                if($item->b_techniques!="") array_push($chapters, "B");

                                // Initialisation array fiche
                                $fiche = array(
                                    'title' => $item->titre_techniques,
                                    'file' => "techniques_".$item->id_techniques.".html",
                                    'coverage' => $item->cible_techniques,
                                    'keywords' => $item->r_techniques,
                                    'authors' => $item->auteur_techniques,
                                    'chapters' => $chapters,
                                    'speciality' => $item->specialite_techniques,
                                    'action' => "ADD"
                                );

                                // On boucle sur les médias de cette fiche
                                $mediasAll = $ezdb->get_results('SELECT * FROM medias WHERE technique_medias REGEXP "(^'.$item->id_techniques.'$)|(^'.$item->id_techniques.',)|(,'.$item->id_techniques.',)|(,'.$item->id_techniques.'$)" ORDER BY ordre_medias ASC');

                                if(isset($mediasAll))
                                {
                                    // Création array medias
                                    $medias = array();

                                    foreach($mediasAll as $media)
                                    {
                                        echo "<ul><li>Media ".$media->id_medias." >> ".$media->nom_medias."</li></ul>";

                                        $mediaArray = array(
                                            'title' => $media->nom_medias,
                                            'subtitle' => $media->legende_medias,
                                            'type' => $media->type_medias,
                                            'thumb' => $media->vignette_medias,
                                            'file' => $media->fichier_medias
                                        );

                                        array_push($medias, $mediaArray);
                                    }

                                    $fiche['medias'] = $medias;
                                }

                                array_push($fiches, $fiche);
                            }

                            $arrayTechniques[$value] = $fiches;

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de fiche pour la <b>lettre $value</b></p>";
                        }
                    }

                    $arrayJSON["techniques"] = $arrayTechniques;
                
                    // FICHES IDE
                    echo "<h2>Traitement des <b>fiches ide</b></h2>";
                    
                    // Init array fiches ide
                    $arrayFichesIDE = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM fiches_ide WHERE titre_fiche_ide REGEXP "^'.$value.'" ORDER BY titre_fiche_ide ASC');

                        if(isset($items))
                        {
                            echo "<p>Traitement des fiches de la <b>lettre $value</b></p>";

                            // Init array de fiches
                            $fiches = array();

                            foreach($items as $item)
                            {
                                echo "<li>Fiche n°$item->id_fiche_ide : $item->titre_fiche_ide</li>";
                                
                                // Initialisation array fiche
                                $fiche = array(
                                    'title' => $item->titre_fiche_ide,
                                    'file' => "fiches-ide_".$item->id_fiche_ide.".html",
                                    'action' => "ADD"
                                );

                                // On boucle sur les médias de cette fiche
                                $mediasAll = $ezdb->get_results('SELECT * FROM medias WHERE memo_medias REGEXP "(^'.$item->id_fiche_ide.'$)|(^'.$item->id_fiche_ide.',)|(,'.$item->id_fiche_ide.',)|(,'.$item->id_fiche_ide.'$)" ORDER BY ordre_medias ASC');

                                if(isset($mediasAll))
                                {
                                    // Création array medias
                                    $medias = array();

                                    foreach($mediasAll as $media)
                                    {
                                        echo "<ul><li>Media ".$media->id_medias." >> ".$media->nom_medias."</li></ul>";

                                        $mediaArray = array(
                                            'title' => $media->nom_medias,
                                            'subtitle' => $media->legende_medias,
                                            'type' => $media->type_medias,
                                            'thumb' => $media->vignette_medias,
                                            'file' => $media->fichier_medias
                                        );

                                        array_push($medias, $mediaArray);
                                    }

                                    $fiche['medias'] = $medias;
                                }
                                
                                array_push($fiches, $fiche);
                            }

                            $arrayFichesIDE[$value] = $fiches;

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de fiche pour la <b>lettre $value</b></p>";
                        }
                    }

                    $arrayJSON["fiches-ide"] = $arrayFichesIDE;
                
                    // MEDIAS
                    echo "<h2>Traitement des <b>medias</b></h2>";
                    
                    // Init array medias
                    $arrayMedias = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM medias WHERE nom_medias REGEXP "^'.$value.'" ORDER BY nom_medias ASC');

                        if(isset($items))
                        {
                            echo "<p>Traitement des medias de la <b>lettre $value</b></p>";

                            // Init array de fiches
                            $fiches = array();

                            foreach($items as $item)
                            {
                                // On récupère toutes les fiches pathologies dans lesquels ce media apparait
                                $listesfichesPathologies = "";
                                
                                // On selectionne toutes les fiches pour lesquel ce media apparait dans au moins un paragraphe
                                $fichesReq = $ezdb->get_results("
                                SELECT id_pathologies FROM pathologies
                                WHERE pi_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR c_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR sp_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR de_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR dd_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR t_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR s_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR d_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR md_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR a_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                ");
                                
                                if(isset($fichesReq))
                                {
                                    foreach($fichesReq as $ficheReq)
                                    {
                                        $listesfichesPathologies .= "pathologies_".$ficheReq->id_pathologies.".html,";
                                    }
                                }
                                else
                                {
                                    $listesfichesPathologies = "";
                                }
                                
                                // On récupère toutes les fiches technologies dans lesquels ce media apparait
                                $listesfichesTechniques = "";
                                
                                // On selectionne toutes les fiches pour lesquel ce media apparait dans au moins un paragraphe
                                $fichesTech = $ezdb->get_results("
                                SELECT id_techniques FROM techniques
                                    WHERE pi_techniques LIKE '%MEDIA_".$item->id_medias."'
                                    OR i_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR ci_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR pm_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR dt_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR pe_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR p_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR c_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR s_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                ");
                                
                                if(isset($fichesTech))
                                {
                                    foreach($fichesTech as $ficheTech)
                                    {
                                        $listesfichesTechniques .= "techniques_".$ficheTech->id_techniques.".html,";
                                    }
                                }
                                else
                                {
                                    $listesfichesTechniques = "";
                                }
                                
                                $extensionMedia = str_replace('.','',strrchr($item->fichier_medias, '.'));

                                switch($extensionMedia)
                                {
                                    Case "mp4":
                                    Case "mov":
                                        $fondMedia = "storebgvideo.png";
                                        $protocoleMedia = "vidp://";
                                        break;
                                    Case "png":
                                    Case "jpg":
                                    Case "jpeg":
                                        $fondMedia = "storebgphoto.png";
                                        $protocoleMedia = "imgp://";
                                        break;
                                    Case "pdf":
                                        $fondMedia = "storebgdoc.png";
                                        $protocoleMedia = "pdfp://";
                                        break;
                                    default:
                                        $fondMedia = "storebgblank.png";
                                        $protocoleMedia = "http://";
                                        break;
                                }
                                
                                if($item->vignette_medias == "")
                                {
                                    $vignetteMedia = $fondMedia; 
                                }
                                else
                                { 
                                    $vignetteMedia = $item->vignette_medias;
                                }
                                
                                echo "<li>Media n°$item->id_medias : $item->nom_medias<br />Pathologies : ".$listesfichesPathologies."<br />Techniques : ".$listesfichesTechniques."</li>";
                                
                                // Initialisation array media
                                $fiche = array(
                                    'title' => $item->nom_medias,
                                    'subtitle' => $item->legende_medias,
                                    'speciality' => $item->specialite_medias,
                                    'type' => $item->type_medias,
                                    'protocol' => $protocoleMedia,
                                    'thumb' => $vignetteMedia,
                                    'file' => $item->fichier_medias,
                                    'pathologies' => $listesfichesPathologies,
                                    'techniques' => $listesfichesTechniques,
                                    'keywords' => $item->tags_medias,
                                    'action' => "ADD"
                                );

                                array_push($fiches, $fiche);
                            }

                            $arrayMedias[$value] = $fiches;

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de media pour la <b>lettre $value</b></p>";
                        }
                    }

                    $arrayJSON["medias"] = $arrayMedias;
                    
                    // GENERATION DES FICHIERS PATHOLOGIES
                    $fiches = $ezdb->get_results("SELECT * FROM pathologies ORDER BY id_pathologies ASC");
                    
                    foreach($fiches as $fiche)
                    {
                        $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.$fiche->titre_pathologies.'</title></head><body><div id="content"><div class="entete"><b>'.id2champ('cibles','label_cibles','id_cibles',$fiche->cible_pathologies).'</b><br />Sp&eacute;cialit&eacute; : '.ligneSpecialites($fiche->specialite_pathologies).'</div><div class="contenu">';

                        if($fiche->pi_pathologies!="")
                        {
                            $contenu .= '<h1><a name="PI"></a>Points importants</h1>'.boutonMedia($fiche->pi_pathologies);
                        }

                        if($fiche->c_pathologies!="")
                        {
                            $contenu .= '<h1><a name="C"></a>Présentation clinique / CIMU</h1>'.boutonMedia($fiche->c_pathologies);
                        }

                        if($fiche->sp_pathologies!="")
                        {
                            $contenu .= '<h1><a name="SP"></a>Signes paracliniques</h1>'.boutonMedia($fiche->sp_pathologies);
                        }

                        if($fiche->de_pathologies!="")
                        {
                            $contenu .= '<h1><a name="DE"></a>Diagnostic étiologique</h1>'.boutonMedia($fiche->de_pathologies);
                        }

                        if($fiche->dd_pathologies!="")
                        {
                            $contenu .= '<h1><a name="DD"></a>Diagnostic différentiel</h1>'.boutonMedia($fiche->dd_pathologies);
                        }

                        if($fiche->t_pathologies!="")
                        {
                            $contenu .= '<h1><a name="T"></a>Traitement</h1>'.boutonMedia($fiche->t_pathologies);
                        }

                        if($fiche->s_pathologies!="")
                        {
                            $contenu .= '<h1><a name="S"></a>Surveillance</h1>'.boutonMedia($fiche->s_pathologies);
                        }

                        if($fiche->d_pathologies!="")
                        {
                            $contenu .= '<h1><a name="D"></a>Devenir / orientation</h1>'.boutonMedia($fiche->d_pathologies);
                        }

                        if($fiche->md_pathologies!="")
                        {
                            $contenu .= '<h1><a name="MD"></a>Mécanisme / description</h1>'.boutonMedia($fiche->md_pathologies);
                        }

                        if($fiche->a_pathologies!="")
                        {
                            $contenu .= '<h1><a name="A"></a>Algorithme</h1>'.boutonMedia($fiche->a_pathologies);
                        }

                        if($fiche->b_pathologies!="")
                        {
                            $contenu .= '<h1><a name="B"></a>Bibliographie</h1>'.boutonMedia($fiche->b_pathologies);
                        }

                        $contenu .= '<p style="font-weight: bold;">Auteur(s) : '.$fiche->auteur_pathologies.'</p></div></div></body></html>';
                        
                        // On ecrit le fichier html
                        forceFilePutContents("ressources/1/pathologies/pathologies_".$fiche->id_pathologies.".html", $contenu);
                        
                        echo "<p>Génération de la <b>fiche n°".$fiche->id_pathologies."</b> avec succès</p>";
                    } // Fin boucle fiches
                
                    // GENERATION DES FICHIERS TECHNIQUES
                    $fiches = $ezdb->get_results("SELECT * FROM techniques ORDER BY id_techniques ASC");
                    
                    foreach($fiches as $fiche)
                    {
                        $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.$fiche->titre_techniques.'</title></head><body><div id="content"><div class="entete"><b>'.id2champ('cibles','label_cibles','id_cibles',$fiche->cible_techniques).'</b><br />Spécialité : '.ligneSpecialites($fiche->specialite_techniques).'</div><div class="contenu">';

                        if($fiche->pi_techniques!="")
                        {
                            $contenu .= '<h1><a name="PI"></a>Points importants</h1>'.boutonMedia($fiche->pi_techniques);
                        }

                        if($fiche->i_techniques!="")
                        {
                            $contenu .= '<h1><a name="I"></a>Indications</h1>'.boutonMedia($fiche->i_techniques);
                        }

                        if($fiche->ci_techniques!="")
                        {
                            $contenu .= '<h1><a name="CI"></a>Contre-indications</h1>'.boutonMedia($fiche->ci_techniques);
                        }

                        if($fiche->pm_techniques!="")
                        {
                            $contenu .= '<h1><a name="PM"></a>Présentation du matériel</h1>'.boutonMedia($fiche->pm_techniques);
                        }

                        if($fiche->dt_techniques!="")
                        {
                            $contenu .= '<h1><a name="DT"></a>Description de la technique</h1>'.boutonMedia($fiche->dt_techniques);
                        }

                        if($fiche->pe_techniques!="")
                        {
                            $contenu .= '<h1><a name="PE"></a>Précautions d’emploi</h1>'.boutonMedia($fiche->pe_techniques);
                        }

                        if($fiche->p_techniques!="")
                        {
                            $contenu .= '<h1><a name="P"></a>Pièges éventuels</h1>'.boutonMedia($fiche->p_techniques);
                        }

                        if($fiche->c_techniques!="")
                        {
                            $contenu .= '<h1><a name="C"></a>Complications</h1>'.boutonMedia($fiche->c_techniques);
                        }

                        if($fiche->s_techniques!="")
                        {
                            $contenu .= '<h1><a name="S"></a>Surveillance</h1>'.boutonMedia($fiche->s_techniques);
                        }

                        if($fiche->b_techniques!="")
                        {
                            $contenu .= '<h1><a name="B"></a>Bibliographie</h1>'.$fiche->b_techniques;
                        }

                        $contenu .= '<p style="font-weight: bold;">Auteur(s) : '.$fiche->auteur_techniques.'</p></div></div></body></html>';
                        
                        //on ecrit le fichier html
                        forceFilePutContents("ressources/1/techniques/techniques_".$fiche->id_techniques.".html", $contenu);
                        
                        echo "<p>Génération de la <b>fiche n°".$fiche->id_techniques."</b> avec succès</p>";
                    } // Fin boucle fiches
                
                    // GENERATION DES FICHIERS FICHES IDE
                    $fiches = $ezdb->get_results("SELECT * FROM fiches_ide ORDER BY id_fiche_ide ASC");
                    
                    foreach($fiches as $fiche)
                    {
                        $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.5, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.$fiche->titre_fiche_ide.'</title></head><body class="fiche-ide"><div id="content"><div class="entete">'.$fiche->titre_fiche_ide.'</div><div class="contenu">';

                        $contenu .= boutonMedia($fiche->contenu_fiche_ide);

                        $contenu .= '</div></div></body></html>';
                        
                        //on ecrit le fichier html
                        forceFilePutContents("ressources/1/fiches-ide/fiches-ide_".$fiche->id_fiche_ide.".html", $contenu);
                        
                        echo "<p>Génération de la <b>fiche n°".$fiche->id_fiche_ide."</b> avec succès</p>";
                    } // Fin boucle fiches
                
                    // GENERATION DES FICHIERS MEDIAS (FICHIERS ET VIGNETTES)
                    $src = 'medias/fichiers/';
                    $dst = 'ressources/1/fichiers/';
                    recurse_copy($src,$dst);
                
                    $src = 'medias/vignettes/';
                    $dst = 'ressources/1/vignettes/';
                    recurse_copy($src,$dst);
                
                    // GENERATION FICHIER AUTEURS.JSON
                    $items = $ezdb->get_results('SELECT * FROM auteurs');

                    // Init array
                    $auteurs = array();
                
                    if(isset($items))
                    {
                        $array = array();
                        
                        foreach($items as $item)
                        {
                            // Initialisation array fiche
                            $auteur = array(
                                'name' => $item->nom_auteurs,
                                'locality' => $item->lieu_auteurs
                            );

                            array_push($array, $auteur);
                        }

                        echo "</ul><p>- - - fin de section</p>";
                    }
                    else
                    {
                        echo "<p>Pas d'auteur</p>";
                    }
                
                    $auteurs['auteurs'] = $array;
                
                    // On ecrit le fichier json
                    forceFilePutContents("ressources/1/ressources/auteurs.json", json_encode($auteurs));
                
                    echo "<p>Fichier <b>auteurs.json</b> généré avec succès.</p> ";
                
                    // GENERATION FICHIER COMITES.JSON
                    $items = $ezdb->get_results('SELECT * FROM comites');

                    // Init array
                    $comites = array();
                
                    if(isset($items))
                    {
                        $array = array();
                        
                        foreach($items as $item)
                        {
                            // Initialisation array comite
                            $comite = array(
                                'name' => $item->nom_comites,
                                'locality' => $item->lieu_comites
                            );

                            array_push($array, $comite);
                        }

                        echo "</ul><p>- - - fin de section</p>";
                    }
                    else
                    {
                        echo "<p>Pas de comité de relecture</p>";
                    }
                
                    $comites['comites'] = $array;
                
                    // On ecrit le fichier json
                    forceFilePutContents("ressources/1/ressources/comites.json", json_encode($comites));
                
                    echo "<p>Fichier <b>comites.json</b> généré avec succès.</p> ";
                
                    // GENERATION DES PAGES A PROPOS
                    $pages = $ezdb->get_results("SELECT * FROM pages");
                    
                    foreach($pages as $page)
                    {
                        $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.$page->titre_pages.'</title></head><body><div class="contenu">';

                        $contenu .= $page->contenu_pages;

                        $contenu .= '</div></body></html>';
                        
                        // On ecrit le fichier html
                        forceFilePutContents("ressources/1/ressources/".$page->nom_fichier_pages, $contenu);
                        
                        echo "<p>Génération du fichier <b>".$page->titre_pages."</b> avec succès</p>";
                    } // Fin boucle fiches
                
                    // RESSOURCES
                
                    // Copy ressources in directory 1
                    $src = 'ressources_bo/';
                    $dst = 'ressources/1/ressources/';
                    recurse_copy($src,$dst);
                    
                    // On selectionne toutes les ressources
                    $ressourcesBD = $ezdb->get_results("SELECT * FROM ressources");

                    // Init array de fiches
                    $ressources = array();

                    foreach($ressourcesBD as $ressourceBD)
                    {
                        // Initialisation array fiche
                        $ressource = array(
                            'action' => "ADD",
                            'file' => $ressourceBD->fichier_ressources
                        );
                        
                        array_push($ressources, $ressource);
                    }
                
                    $arrayJSON["ressources"] = $ressources;
                
                    
                    // GENERATION ARCHIVE ZIP
                    try 
                    {
                        // On ecrit le fichier json
                        forceFilePutContents("ressources/1/1.json", json_encode($arrayJSON));

                        echo "<p>Fichier <b>1.json</b> généré avec succès.</p>";
                    } 
                    catch (Exception $e) 
                    {
                        echo 'Erreur lors de la création du json : ',  $e->getMessage(), "\n";
                    }
                    
                    include('includes/pclzip.lib.php');
                    $archive = new PclZip('ressources/ressources.zip');

                    $v_list = $archive->add('ressources/1', PCLZIP_OPT_REMOVE_PATH, 'ressources/1');

                    echo "<p style='text-align:center;margin:10px;'><a class='pme-add' style='width:500px;color:#FFFFFF;'href='ressources/ressources.zip' target='_blank'>  >> Télécharger le fichier ressources.zip' <<  </a></p>";

                    echo "<p><a class='pme-view' style='width:200px;color:#FFFFFF;'href='export.php' target='_blank'><< retour</a></p>";
                ?>
            </div>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>