<?php
    require_once 'includes/access.class.php';

    $user = new flexibleAccess();

    if (isset($_GET['logout']) && $_GET['logout'] == 1)
    {
        $user->logout('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
    }
    if (!$user->is_loaded())
    {
        if (isset($_POST['uname']) && isset($_POST['pwd']))
        {
            if (!$user->login($_POST['uname'], $_POST['pwd']))
            {
                echo '<p style="font-family:sans-serif;text-align:center;">Mauvaise identification.</p>';
            }
            else
            {
                // User is now loaded
                header("Location: accueil.php");
            }
        }
        
        include("includes/login.php");
    }
    else
    {
        //User is loaded
        header("Location: accueil.php");
    }
?>