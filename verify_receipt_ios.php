<?php
    // Get data with $_POST
    $receiptData = $_POST['receipt-data'];
    $password = $_POST['password'];
    $isSandbox = "true";

    // Determine which endpoint to use for verifying the receipt
    if ($isSandbox == "true") 
        $endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
    else
        $endpoint = 'https://buy.itunes.apple.com/verifyReceipt';

    // Build the post data
    $postData = json_encode(
        array('receipt-data' => $receiptData, 'password' => $password)
    );

    // Create the cURL request
    $ch = curl_init($endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    // Execute the cURL request and fetch response data
    $response = curl_exec($ch);
    $errno    = curl_errno($ch);
    $errmsg   = curl_error($ch);

    curl_close($ch);
    
    // Ensure the request succeeded
    if ($errno != 0) {
        $result = array('result' => 'false');
        echo json_encode($result);
        return;
    }

    // Parse the response data
    $data = json_decode($response);

    // Ensure response data was a valid JSON string
    if (!is_object($data)) 
    {
        $result = array('result' => 'false');
        echo json_encode($result);
        return;
    }

    // Ensure the expected data is present
    if (!isset($data->status) || $data->status != 0) 
    {
        $result = array('result' => 'false');
        echo json_encode($result);
        return;
    }
    
    // Check expiration date
    $latest_expiration_interval_since_1970 = 0;
    
    foreach($data->latest_receipt_info as $receipt_info)
    {
        $expiration_interval_since_1970 = intval(doubleval($receipt_info->expires_date_ms) / 1000.0);
            
        // Save last date expiration of all purchases
        if ($expiration_interval_since_1970 > $latest_expiration_interval_since_1970) 
        {
            $latest_expiration_interval_since_1970 = $expiration_interval_since_1970;
        }
    }

    if ($latest_expiration_interval_since_1970 > time())
    {
        $result = array('result' => 'true', 'expiration_date' => $latest_expiration_interval_since_1970);
        echo json_encode($result);
    }
    else
    {
        $result = array('result' => 'false');
        echo json_encode($result);
    }
?>