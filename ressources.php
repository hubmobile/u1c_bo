<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'ressources';
    // Champ clé unique
    $opts['key'] = 'id_ressources';
    // Tri par défaut
    $opts['sort_field'] = array('id_ressources');
    // Boutons navigation
    $opts['buttons']['L']['up'] = array('add');
    // Triggers
    $opts['triggers']['insert']['before']  = 'ressources_IB.php';
    $opts['triggers']['update']['before']  = 'ressources_IB.php';
    // Champs de la table
    $opts['fdd']['id_ressources'] = array(
        'name'     => 'ID',
        'select'   => 'T',
        'options'  => 'LFVCPDR', // auto increment
        'maxlen'   => 11,
        'default'  => '0',
        'sort'     => true
    );
    $opts['fdd']['id_ressources']['css'] = array('postfix' => 'ColId');

    $opts['fdd']['fichier_ressources'] = array(
        'name'     => 'Fichier',
        'select'   => 'T',
        'maxlen'   => 255
    );

    $opts['fdd']['dataset'] = array(
        'name'     => 'Dataset',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true,
        'default' => 1
    );

    // On selectionne tous les datasets
    $items = $ezdb->get_results('SELECT * FROM datasets');

    $datasetsAll = array();

    foreach($items as $item) 
    { 
        $datasetsAll[$item->version] = $item->version;
    }

    $opts['fdd']['dataset']['values2'] = $datasetsAll;

    $opts['fdd']['action'] = array(
        'name'     => 'Action',
        'select'   => 'D',
        'sort'     => true
    );

    $opts['fdd']['action']['values2'] = array(
        'ADD' => 'Ajouter',
        'DEL' => 'Supprimer',
        'UP' => 'Modifier'
    ); 
?>

<script type="text/javascript">
    $(document).ready(function() {
        $("input[name='PME_data_fichier_ressources']").replaceWith("<input type='file' name='PME_data_fichier_ressources_fichier' /><br /><b>Fichier existant en base :</b>  " + $("input[name='PME_data_fichier_ressources']").val() + "</div><input type='hidden' name='PME_data_fichier_ressources' value='" + $("input[name='PME_data_fichier_ressources']").val() + "' />");
    });
</script>

<div id="middle">
    <div id="left-column">
        <h3>Ressource</h3>
        <ul class="nav">
            <li><i>Permet de déposer les fichiers ressources présents dans les fiches (style.css, images, etc...)</i></li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Ressources</h1>
        </div>
        <div class="table">
            <?php
            // call to phpMyEdit
            require_once 'phpMyEdit.class.php';
            new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>

