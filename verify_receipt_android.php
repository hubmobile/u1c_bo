<?php
    require_once 'includes/google/vendor/autoload.php';

    // Get data with $_POST
    $signedData = $_POST['signedData'];
    $signature = $_POST['signature'];

    $client = new \Google_Client();
    $client->setAuthConfig('credentials_seditions.json');
    $client->addScope('https://www.googleapis.com/auth/androidpublisher');
    $service = new \Google_Service_AndroidPublisher($client);

    $json = json_decode($signedData, true);

    $purchase = $service->purchases_subscriptions->get($json['packageName'], $json['productId'], $json['purchaseToken']);

    if (!$purchase || $purchase["error"] != null) 
    {
        $result = array('result' => 'false');
        echo json_encode($result);
    }

    $expiration_interval_since_1970 = intval(doubleval($purchase["expiryTimeMillis"] / 1000.0));

    if ($expiration_interval_since_1970 > time())
    {
        $result = array('result' => 'true', 'expiration_date' => $expiration_interval_since_1970);
        echo json_encode($result);
    }
    else
    {
        $result = array('result' => 'false');
        echo json_encode($result);
    }
?>