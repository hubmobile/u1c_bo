<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Ebook Urgences en 1 clic - Authentification</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/all.css" />
    </head>
    <body class="login">
        <div class="login_content">
            <img class="logo" src="img/logo.png" alt=""/>
            <p>
                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <table>
                        <tr>
                            <td>Identifiant : </td>
                            <td><input type="text" name="uname" /></td>
                        </tr>

                        <tr>
                            <td>Mot de passe : </td>
                            <td><input type="password" name="pwd" /></td>
                        </tr>

                        <tr>
                            <td>Se souvenir de moi ? </td>
                            <td><input type="checkbox" name="remember" value="1" /></td>
                        </tr>
                    </table>
                    
                    <input class="button_submit" type="submit" value="Se connecter" />
                </form>
            </p>
        </div>
    </body>
</html>