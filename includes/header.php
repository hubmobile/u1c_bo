<?php
    include("access.class.php");

    $user = new flexibleAccess();

    if ( !$user->is_loaded() )
        header("Location: index.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Ebook Urgences en 1 clic</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/all.css" />
        <link rel="stylesheet" type="text/css" href="css/pme.css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/ckeditor/adapters/jquery.js"></script>
    </head>
    <body>
        <div id="main">
            <div id="header">
                <a href="index.php" class="logo"><img src="img/logo.png" alt=""/></a>
                <ul id="top-navigation">
                    <li class="<?php classmenutop("pathologies");?>"><span><span><a href="pathologies.php">Pathologies</a></span></span></li>
                    <li class="<?php classmenutop("techniques");?>"><span><span><a href="techniques.php">Techniques</a></span></span></li>
                    <li class="<?php classmenutop("medias");?>"><span><span><a href="medias.php">Medias</a></span></span></li>
                    <li class="<?php classmenutop("fiches_ide");?>"><span><span><a href="fiches_ide.php">Fiches IDE</a></span></span></li>
                    <li class="<?php classmenutop("auteurs");?>"><span><span><a href="auteurs.php">Auteurs</a></span></span></li>
                    <li class="<?php classmenutop("comites");?>"><span><span><a href="comites.php">Comité de relecture</a></span></span></li>
                    <li class="<?php classmenutop("ressources");?>"><span><span><a href="ressources.php">Ressources</a></span></span></li>
                    <li class="<?php classmenutop("pages");?>"><span><span><a href="pages.php">Pages</a></span></span></li>
                    <li class="<?php classmenutop("export");?>"><span><span><a href="export.php">Exportation</a></span></span></li>
                    <li class="<?php classmenutop("datasets");?>"><span><span><a href="datasets.php">Datasets</a></span></span></li>
                    <li class="<?php classmenutop("notifications");?>"><span><span><a href="notifications.php">Notifications</a></span></span></li>
                </ul>
            </div>