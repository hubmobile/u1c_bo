<?php
//fonction ID to Label
function id2label($table,$id) {
	global $ezdb;
	$label = $ezdb->get_var("SELECT label FROM $table WHERE id = '$id'");
	return $label;
}

//fonction ID to Champs
function id2champ($table,$champ,$id,$idvalue) {
	global $ezdb;
	$label = $ezdb->get_var("SELECT $champ FROM $table WHERE $id = '$idvalue'");
	return $label;
}

//nettoyage import xml
function supprimeTags($valeurInput){
  $tags = array("<id_pathologies>", "</id_pathologies>", "<titre_pathologies>", "</titre_pathologies>", "<abreviation>", "</abreviation>", "<signes_et_symptomes>", "<signes_et_symptomes>", "<pi_pathologies>", "</pi_pathologies>", "<c_pathologies>", "</c_pathologies>", "<sp_pathologies>", "</sp_pathologies>", "<de_pathologies>", "</de_pathologies>", "<dd_pathologies>", "</dd_pathologies>", "<t_pathologies>", "</t_pathologies>", "<s_pathologies>", "</s_pathologies>", "<d_pathologies>", "</d_pathologies>", "<md_pathologies>", "</md_pathologies>", "<memo>", "</memo>", "<a_pathologies>", "</a_pathologies>", "<b_pathologies>", "</b_pathologies>");
	$valeurOutput = str_replace($tags, "", $valeurInput);
	return $valeurOutput;
}

//function nettoyageAccents($string){
//	return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
//'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
//}

//function supprimerAccents($toClean) {
//
//$liste = array(
//    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
//    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
//    'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
//    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
//    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
//    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
//    'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f'
//);
//    return strtr($toClean, $liste);
//}

//function accentsToHtml($string)
//{
//    $string = str_replace("À", "&Agrave;", $string);
//    $string = str_replace("Á", "&Aacute;", $string);
//    $string = str_replace("Â", "&Acirc;", $string);
//    $string = str_replace("Ã", "&Atilde;", $string);
//    $string = str_replace("Ä", "&Auml;", $string);
//    $string = str_replace("Å", "&Aring;", $string);
//    $string = str_replace("Æ", "&Aelig;", $string);
//    $string = str_replace("à", "&agrave;", $string);
//    $string = str_replace("á", "&aacute;", $string);
//    $string = str_replace("â", "&acirc;", $string);
//    $string = str_replace("ã", "&atilde;", $string);
//    $string = str_replace("ä", "&auml;", $string);
//    $string = str_replace("å", "&aring;", $string);
//    $string = str_replace("æ", "&aelig;", $string);
//    $string = str_replace("Ç", "&Ccedil;", $string);
//    $string = str_replace("ç", "&ccedil;", $string);
//    $string = str_replace("Ð", "&ETH;", $string);
//    $string = str_replace("ð", "&eth;", $string);
//    $string = str_replace("È", "&Egrave;", $string);
//    $string = str_replace("É", "&Eacute;", $string);
//    $string = str_replace("Ê", "&Ecirc;", $string);
//    $string = str_replace("Ë", "&Euml;", $string);
//    $string = str_replace("è", "&egrave;", $string);
//    $string = str_replace("é", "&eacute;", $string);
//    $string = str_replace("ê", "&ecirc;", $string);
//    $string = str_replace("ë", "&euml;", $string);
//    $string = str_replace("Ì", "&Igrave;", $string);
//    $string = str_replace("Í", "&Iacute;", $string);
//    $string = str_replace("Î", "&Icirc;", $string);
//    $string = str_replace("Ï", "&Iuml;", $string);
//    $string = str_replace("ì", "&igrave;", $string);
//    $string = str_replace("í", "&iacute;", $string);
//    $string = str_replace("î", "&icirc;", $string);
//    $string = str_replace("ï", "&iuml;", $string);
//    $string = str_replace("Ñ", "&Ntilde;", $string);
//    $string = str_replace("ñ", "&ntilde;", $string);
//    $string = str_replace("Ò", "&Ograve;", $string);
//    $string = str_replace("Ó", "&Oacute;", $string);
//    $string = str_replace("Ô", "&Ocirc;", $string);
//    $string = str_replace("Õ", "&Otilde;", $string);
//    $string = str_replace("Ö", "&Ouml;", $string);
//    $string = str_replace("Ø", "&Oslash;", $string);
//    $string = str_replace("Œ", "&OElig;", $string);
//    $string = str_replace("ò", "&ograve;", $string);
//    $string = str_replace("ó", "&oacute;", $string);
//    $string = str_replace("ô", "&ocirc;", $string);
//    $string = str_replace("õ", "&otilde;", $string);
//    $string = str_replace("ö", "&ouml;", $string);
//    $string = str_replace("ø", "&oslash;", $string);
//    $string = str_replace("œ", "&oelig;", $string);
//    $string = str_replace("Ù", "&Ugrave;", $string);
//    $string = str_replace("Ú", "&Uacute;", $string);
//    $string = str_replace("Û", "&Ucirc;", $string);
//    $string = str_replace("Ü", "&Uuml;", $string);
//    $string = str_replace("ù", "&ugrave;", $string);
//    $string = str_replace("ú", "&uacute;", $string);
//    $string = str_replace("û", "&ucirc;", $string);
//    $string = str_replace("ü", "&uuml;", $string);
//    $string = str_replace("Ý", "&Yacute;", $string);
//    $string = str_replace("Ÿ", "&Yuml;", $string);
//    $string = str_replace("ý", "&yacute;", $string);
//    $string = str_replace("ÿ", "&yuml;", $string);
//    return $string;
//}
//function caracteresToHtml($string)
//{
//    $string = str_replace("Æ", "&Aelig;", $string);
//    $string = str_replace("æ", "&aelig;", $string);
//    $string = str_replace("Œ", "&OElig;", $string);
//    $string = str_replace("œ", "&oelig;", $string);
//    return $string;
//}

function ligneSpecialites($listeDesSpecialites){
	$specialites = explode(",", $listeDesSpecialites);
	$contenu = "";
	foreach($specialites as $specialite){
		$contenu .= id2champ('specialites','label_specialites','id_specialites',$specialite)." / ";
	}
	return $contenu;
}

function boutonMedia($valeurParag){
  global $ezdb;
  preg_match_all("#\#MEDIA_(\d*)\##i", $valeurParag, $matches, PREG_SET_ORDER);
  foreach ($matches as $val) {
	  $modeleMotif = "#\#MEDIA_".$val[1]."\##i";
		$idMedia = $val[1];
		//on sélectionne les infos du média concerné
	  $infosMedia = $ezdb->get_row("SELECT * FROM medias WHERE id_medias='".$idMedia."'");
	  $extensionMedia = str_replace('.','',strrchr($infosMedia->fichier_medias, '.'));

	switch($extensionMedia){
		Case "mp4":
		Case "mov":
      $fondMedia = "storebgvideo.png";
      $protocoleMedia = "vidp://";
		break;
		Case "png":
		Case "jpg":
		Case "jpeg":
      $fondMedia = "storebgphoto.png";
      $protocoleMedia = "imgp://";
		break;
		Case "pdf":
      $fondMedia = "storebgdoc.png";
      $protocoleMedia = "pdfp://";
		break;
		default:
		  $fondMedia = "storebgblank.png";
		  $protocoleMedia = "http://";
		break;
	}

	switch($infosMedia->type_medias){
  	Case "img":
      $typeMedia = "Image";
		break;
		Case "pho":
      $typeMedia = "Photo";
		break;
  	Case "tab":
      $typeMedia = "Tableau";
		break;
		Case "alg":
      $typeMedia = "Algorithme";
		break;
  	Case "pdf":
      $typeMedia = "Document Pdf";
		break;
		default:
    	$typeMedia = "- - -";
		break;
	}

	if($infosMedia->vignette_medias == ""){$vignetteMedia=$fondMedia;}else{$vignetteMedia=$infosMedia->vignette_medias;}
  
    $modeleLien = "<ul class='pageitem'>
	 <li class='store' style='background-color:#F4F4F4;' >
	 <a href='".$protocoleMedia."".$infosMedia->fichier_medias."'>
	 <span style='background-image: url($vignetteMedia);' class='image'> </span>
	 <span class='comment'>$typeMedia</span>
	 <span class='name'>$infosMedia->nom_medias</span>
	 <span class='arrow'></span>
	 </a>
	 </li>
	 </ul>";

	 $valeurParag = preg_replace($modeleMotif, $modeleLien, $valeurParag);
  }

  return $valeurParag;

}

//function nettoyageUtf($string){
//	// éléments à remplacer
//	$trans = array(
//		"&#39;" => "'",
//		"&OElig;" => "oe",
//		"&oelig;" => "oe",
//		"Â« " => '"',
//		" Â»" => '"',
//		"Ã¢" => "â",
//		"Ã¨" => "è",
//		"Ã©" => "é",
//		"Ã«" => "ë",
//		"Ãª" => "ê",
//		"Ã¯" => "ï",
//		"Ã´" => "ô",
//		"Ã¶" => "ö",
//		"Ã¹" => "ù",
//		"Ã»" => "û",
//		"Ã§" => "ç",
//		"Ã" => "à",
//		"Â®" => "®",
//		"Âœ" => "œ",
//		"Â°" => "°",
//		"Â™" => "",
//		"Â" => "",
//		"à " => "à",
//		">" => "sup.",
//		"<" => "inf.",
//		"â€™" => "'"
//	);
//	$stringClean = strtr($string, $trans);
//
//	return $stringClean;
//}

function insertMedia($valeurParag){
    global $ezdb;
    preg_match_all("#\#MEDIA_(\d*)\##i", $valeurParag, $matches, PREG_SET_ORDER);

    foreach ($matches as $val) {
        $modeleMotif = "#\#MEDIA_".$val[1]."\##i";
        $idMedia = $val[1];
        //on sélectionne les infos du média concerné
        $infosMedia = $ezdb->get_row("SELECT * FROM medias WHERE id_medias='".$idMedia."'");
        $extensionMedia = str_replace('.','',strrchr($infosMedia->fichier_medias, '.'));

        switch($extensionMedia){
                Case "mp4":
                Case "mov":
                $fondMedia = "storebgvideo.png";
                $protocoleMedia = "../../../medias/vignettes/" ;
                $mediaUrl =  $infosMedia->vignette_medias; 
                break;
                Case "png":
                Case "jpg":
                Case "jpeg":
                $fondMedia = "storebgphoto.png";
                $protocoleMedia = "../../../medias/fichiers/";
                $mediaUrl =  $infosMedia->fichier_medias ;
                break;
                Case "pdf":
                $fondMedia = "storebgdoc.png";
                $protocoleMedia = "../../../medias/fichiers/";
                $mediaUrl =  $infosMedia->fichier_medias ;
                break;
            default:
                $fondMedia = "storebgblank.png";
                $protocoleMedia = "../../../medias/vignettes/" ;
                $mediaUrl =  $infosMedia->vignette_medias; 
                break;
        }

        if($infosMedia->vignette_medias == "")
            $vignetteMedia = $fondMedia;
        else
            $vignetteMedia = $infosMedia->vignette_medias;

        if( $protocoleMedia != "none")
        {   
            $fileName = $protocoleMedia.$mediaUrl ;  

            $modeleLien = "<ul >
	 <li  >
      <span class='name'>$infosMedia->nom_medias</span>
	 <img src='".$fileName."'>
	 <span style='background-image: url($mediaUrl);' class='image'> </span>	
	 </li>
	 </ul></BR>"; }
        $valeurParag = preg_replace($modeleMotif, $modeleLien, $valeurParag);




    }
    return $valeurParag;
}

?>