<?php
// ################################ infos database
	$opts['hn'] = 'localhost';
	$opts['un'] = 'root';
	$opts['pw'] = 'root';
	$opts['db'] = 'app_urgences';

// ################################ infos PME
$opts['key_type'] = 'int';

// Number of records to display on the screen
// Value of -1 lists all records in a table
$opts['inc'] = 50;

// Options you wish to give the users
// A - add,  C - change, P - copy, V - view, D - delete,
// F - filter, I - initial sort suppressed
$opts['options'] = 'ACPVDF';

// Number of lines to display on multiple selection filters
$opts['multiple'] = '5';

// Navigation style: B - buttons (default), T - text links, G - graphic links
// Buttons position: U - up, D - down (default)
$opts['navigation'] = 'GBUD';

$opts['buttons']['L']['up'] = array('-add');
$opts['buttons']['L']['down'] = array('-<<','-<','-add','-view','-change','-copy','-delete','->','->>','-goto_combo');
$opts['buttons']['F']['up'] = $opts['buttons']['L']['up'];
$opts['buttons']['A']['up'] = array('');
$opts['buttons']['C']['up'] = $opts['buttons']['A']['up'];
$opts['buttons']['P']['up'] = $opts['buttons']['A']['up'];
$opts['buttons']['D']['up'] = $opts['buttons']['A']['up'];
$opts['buttons']['V']['up'] = $opts['buttons']['A']['up'];

// Display special page elements
$opts['display'] = array(
	'form'  => true,
	'query' => true,
	'sort'  => true,
	'time'  => true,
	'tabs'  => true
);

// Set default prefixes for variables
$opts['js']['prefix']               = 'PME_js_';
$opts['dhtml']['prefix']            = 'PME_dhtml_';
$opts['cgi']['prefix']['operation'] = 'PME_op_';
$opts['cgi']['prefix']['sys']       = 'PME_sys_';
$opts['cgi']['prefix']['data']      = 'PME_data_';

/* Get the user's default language and use it if possible or you can
   specify particular one you want to use. Refer to official documentation
   for list of available languages. */
$opts['language'] = 'FR-UTF8';

$opts['display']['time'] = "";


// ################################ function pages

// nom du fichier
$nomfichier = basename ($_SERVER['PHP_SELF'],".php");
// gestion des class du menu top
function classmenutop($nomdufichier){
global $nomfichier;
if($nomdufichier==$nomfichier){echo "active";}
}
?>