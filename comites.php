<?php
    include("includes/config.php");
    include("includes/header.php");

    // Table
    $opts['tb'] = 'comites';
    // Champ clé unique
    $opts['key'] = 'id_comites';
    // Tri par défaut
    $opts['sort_field'] = array('id_comites');
    // boutons navigation
    $opts['buttons']['L']['up'] = array('add');

    //champs de la table
    $opts['fdd']['id_comites'] = array(
        'name'     => 'ID',
        'select'   => 'T',
        'options'  => 'VCPDR', // auto increment
        'maxlen'   => 11,
        'default'  => '0',
        'sort'     => true
    );
    $opts['fdd']['nom_comites'] = array(
        'name'     => 'Nom',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true
    );
    $opts['fdd']['lieu_comites'] = array(
        'name'     => 'Lieu',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true
    );
?>

<div id="middle">
    <div id="left-column">
        <h3>Comité de relecture</h3>
        <ul class="nav">
            <li>Permet de saisir la liste du comité de relecture des fiches de l'application</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Comité de relecture</h1>
        </div>
        <div class="table">
            <?php
            // call to phpMyEdit
            require_once 'phpMyEdit.class.php';
            new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>