<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'techniques';
    // Champ clé unique
    $opts['key'] = 'id_techniques';
    // Tri par défaut
    $opts['sort_field'] = array('id_techniques');
    // boutons navigation
    $opts['buttons']['L']['up'] = array('add');

    //champs de la table

    $opts['fdd']['id_techniques'] = array(
        'name'     => 'ID',
        'select'   => 'T',
        'options'  => 'LFVCPDR', // auto increment
        'maxlen'   => 11,
        'default'  => '0',
        'sort'     => true
    );
    $opts['fdd']['id_techniques']['css'] = array('postfix' => 'ColId');

    $opts['fdd']['statut_techniques'] = array(
        'name'     => 'Statut de la fiche',
        'select'   => 'D',
        'sort'     => true
    );
    $opts['fdd']['statut_techniques']['values2'] = array(
        '1' => 'Fiche Validée',
        '3' => 'Fiche revue par PP',
        '4' => 'Fiche revue par AD',
        '2' => 'Fiche à relire',
        '0' => 'Fiche en cours'
    );

    $opts['fdd']['titre_techniques'] = array(
        'name'     => 'Titre de la fiche',
        'select'   => 'T',
        'options'  => 'AVCPDLF',
        'maxlen'   => 255,
        'sort'     => true
    );

    $opts['fdd']['cible_techniques'] = array(
        'name'     => 'Cible',
        'select'   => 'D',
        'sort'     => true
    );
    $opts['fdd']['cible_techniques']['values2'] = array(
        '1' => 'Adulte',
        '2' => 'Enfant',
        '3' => 'Adulte & Enfant'
    );

    $opts['fdd']['specialite_techniques'] = array(
        'name'     => 'Spécialité',
        'select'   => 'M',
        'sort'     => true
    );
    $opts['fdd']['specialite_techniques']['values2'] = array(
        '2' => 'cardiologie',
        '3' => 'dermatologie',
        '4' => 'endocrinologie',
        '5' => 'gastro-entérologie',
        '7' => 'génito-urinaire',
        '6' => 'gynécologie',
        '8' => 'hématologie',
        '1' => 'infectieux',
        '9' => 'métabolisme',
        '10' => 'neurologie',
        '11' => 'ophtalmologie',
        '12' => 'orl',
        '13' => 'pathologies circonstancielles',
        '16' => 'pédiatrie',
        '14' => 'pneumologie',
        '15' => 'psychiatrie',
        '17' => 'rhumatologie',
        '18' => 'stomatologie',
        '19' => 'symptômes',
        '20' => 'système immunitaire',
        '21' => 'toxicologie',
        '22' => 'traumatologie',
        '23' => 'vasculaire'
    );

    $opts['fdd']['pi_techniques'] = array(
        'name'     => 'Points importants',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['pi_techniques']['escape'] = false;

    $opts['fdd']['i_techniques'] = array(
        'name'     => 'Indications',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['i_techniques']['escape'] = false;

    $opts['fdd']['ci_techniques'] = array(
        'name'     => 'Contre indications',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['ci_techniques']['escape'] = false;

    $opts['fdd']['pm_techniques'] = array(
        'name'     => 'Présentation du matériel',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['pm_techniques']['escape'] = false;

    $opts['fdd']['dt_techniques'] = array(
        'name'     => 'Description de la technique',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['dt_techniques']['escape'] = false;

    $opts['fdd']['pe_techniques'] = array(
        'name'     => "Précautions d'emploi",
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['pe_techniques']['escape'] = false;

    $opts['fdd']['p_techniques'] = array(
        'name'     => 'Pièges éventuels',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['p_techniques']['escape'] = false;

    $opts['fdd']['c_techniques'] = array(
        'name'     => 'Complications',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['c_techniques']['escape'] = false;

    $opts['fdd']['s_techniques'] = array(
        'name'     => 'Surveillance',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['s_techniques']['escape'] = false;

    $opts['fdd']['b_techniques'] = array(
        'name'     => 'Bibliographie',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['b_techniques']['escape'] = false;

    $opts['fdd']['r_techniques'] = array(
        'name'     => 'Tags',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 120)
    );

    $opts['fdd']['auteur_techniques'] = array(
        'name'     => 'Auteur',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 255
    );

    $opts['fdd']['liens_techniques'] = array(
        'name'     => 'Liens',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['liens_techniques']['escape'] = false;
    
    $opts['fdd']['dataset'] = array(
        'name'     => 'Dataset',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true,
        'default' => 1
    );

    // On selectionne tous les datasets
    $items = $ezdb->get_results('SELECT * FROM datasets');

    $datasetsAll = array();

    foreach($items as $item) 
    { 
        $datasetsAll[$item->version] = $item->version;
    }

    $opts['fdd']['dataset']['values2'] = $datasetsAll;

    $opts['fdd']['action'] = array(
        'name'     => 'Action',
        'select'   => 'D',
        'sort'     => true
    );

    $opts['fdd']['action']['values2'] = array(
        'ADD' => 'Ajouter',
        'DEL' => 'Supprimer',
        'UP' => 'Modifier'
    ); 
?>

<script type="text/javascript">
    $(function()
      {
        var config = {
            toolbar:   'ToolbarUrgences'
        };

        // Initialize the editor.
        // Callback function can be passed and executed after full instance creation.
        $("textarea[name='PME_data_pi_techniques']").ckeditor(config);
        $("textarea[name='PME_data_i_techniques']").ckeditor(config);
        $("textarea[name='PME_data_ci_techniques']").ckeditor(config);
        $("textarea[name='PME_data_pm_techniques']").ckeditor(config);
        $("textarea[name='PME_data_dt_techniques']").ckeditor(config);
        $("textarea[name='PME_data_pe_techniques']").ckeditor(config);
        $("textarea[name='PME_data_p_techniques']").ckeditor(config);
        $("textarea[name='PME_data_c_techniques']").ckeditor(config);
        $("textarea[name='PME_data_s_techniques']").ckeditor(config);
        $("textarea[name='PME_data_b_techniques']").ckeditor(config);
        $("textarea[name='PME_data_liens_techniques']").ckeditor(config);
    });
</script>

<div id="middle">
    <div id="left-column">
        <h3>Techniques</h3>
        <ul class="nav">
            <li>Permet de saisir les fiches Techniques</li>
            <li><b>Insertion Média :</b>
                <br />Pour insérer le Média ID 32, saisir #MEDIA_32# dans le texte du paragraphe</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Techniques</h1>
        </div>
        <div class="table">
            <?php
            // call to phpMyEdit
            require_once 'phpMyEdit.class.php';
            new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>
<?php
include("includes/footer.php");
?>

