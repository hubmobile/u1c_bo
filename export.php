<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");
    include("includes/fonctions.php");
?>

<div id="middle">
    <div id="left-column">
        <h3>Exportation</h3>
        <ul class="nav">
            <li>Cette page permet de générer les fichiers nécessaires à l'application mobile</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="table">

            <div>
                <div class="top-bar"><h1>Exportation des fichiers</h1></div>

                <div style="clear:both;">&nbsp;</div>

                <p class="warning_export"><b>Attention :</b> cette rubrique est réservée aux administrateurs</p>

                <div style="padding: 10px;">
                    <h2>Exportation du fichier ZIP (ressources.zip) en version 1</h2>

                    <form action="export_json.php" method="post">
                        <input type="submit" name="exporter" id="exporter" class="pme-add" style="display:inline;" value="Générer" />
                    </form>
                </div>
                
                <div style="padding: 10px;">
                    <h2>Exportation du dataset en version : </h2>
                    
                    <form action="export_dataset.php" method="post">
                        <select name="select_dataset" required style="float: left;" >

                            <?php 
                                // On selectionne tous les datasets
                                $items = $ezdb->get_results('SELECT * FROM datasets ORDER BY version ASC');

                                foreach($items as $item) 
                                { 
                                    ?>
                                        <option <?php if ($item->version == 1 ) echo 'disabled'; ?> value="<?php echo $item->version; ?>"><?php echo $item->version; ?></option>
                                    <?php
                                }
                            ?>
                        </select>

                    
                        <input type="submit" name="exporter" id="exporter" class="pme-add" style="display:inline;" value="Générer" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<?php
    include("includes/footer.php");
?>