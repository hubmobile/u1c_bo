<?php
    // On test s'il y a un fichier à uploader par la présence de son nom
    if($_FILES['PME_data_fichier_ressources_fichier']['name'] != "")
    {
        echo "<p class='msg_vert'>Il y a un fichier à charger : ".$_FILES['PME_data_fichier_ressources_fichier']['name']."</p>";
    }
    else
    {
        echo "<p class='msg_jaune'>Il n'y a pas de fichier à charger : ".$_FILES['PME_data_fichier_ressources_fichier']['name']."</p>";
    }
   
    if($_FILES['PME_data_fichier_ressources_fichier']['name'] != "")
    {
        $fichierOrigine = $_FILES['PME_data_fichier_ressources_fichier']['name'];
        $fichierChemin = pathinfo($fichierOrigine);
        $fichierExtension = $fichierChemin['extension'];
        $fichierAutorisees = array("jpeg","jpg","mov","mp4","png","pdf", "css");
        $fichierDestination = dirname(__FILE__)."/ressources_bo/";
        
        // Traitement du fichier
        // On récupère l'ID de la ressource concernée avec $this->rec pour fabriquer le nom du fichier
        if (!(in_array($fichierExtension, $fichierAutorisees))) 
        {
            echo "<p class='msg_rouge'>Le fichier ".$_FILES['PME_data_fichier_ressources_fichier']['name']." n'a pas l'extension attendue</p>";
        } 
        else 
        {
            if (move_uploaded_file($_FILES["PME_data_fichier_ressources_fichier"]["tmp_name"],$fichierDestination.$fichierOrigine)) 
            {
                echo "<p class='msg_vert'>Le fichier temporaire ".$_FILES["PME_data_fichier_ressources_fichier"]["tmp_name"]." a été déplacé vers ".$fichierDestination.$fichierOrigine."</p>";
            } 
            else 
            {
                echo "<p class='msg_jaune'>Le fichier n'a pas été uploadé car il est trop gros.</p>";
            }
        }
        
        // On affecte le nom du fichier
        $newvals['fichier_ressources'] = $fichierOrigine;
    }
?>