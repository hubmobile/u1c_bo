<?php
    include("includes/ezsql.php");

    // Get version app with $_POST
    $userVersion = $_POST['userVersion'];

    $dataset = $ezdb->get_results('SELECT * FROM datasets WHERE actif = 1 LIMIT 1');
    
    // Init all libelles string 
    $allLibelles = "";

    while ($userVersion <= $dataset[0]->version)
    {     
        // Increment user version
        $userVersion++;
        
        $datasetIncrement = $ezdb->get_row("SELECT * FROM datasets WHERE version = '".$userVersion."'");

        if (isset($datasetIncrement->libelle ))
            $allLibelles .= $datasetIncrement->libelle . "\r\n\r\n";
    }

    if(isset($dataset))
    {
        $result = array('version' => $dataset[0]->version, 'libelles' => $allLibelles);
        echo json_encode($result);
    } 
?>