<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'notifications';
    // Champ clé unique
    $opts['key'] = 'id';
    // Tri par défaut
    $opts['sort_field'] = array('id');
    // boutons navigation
    $opts['buttons']['L']['up'] = array('add');
    $opts['buttons']['L']['down'] = array('add');

    // Triggers
    $opts['triggers']['insert']['after']  = 'send_notification.php';

    //champs de la table
    $opts['fdd']['id'] = array(
        'name'     => 'ID',
        'select'   => 'T',
        'options'  => 'LFVCPDR',
        'maxlen'   => 11,
        'default'  => '0',
        'sort'     => true
    );

    $opts['fdd']['title'] = array(
        'name'     => 'Titre',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true
    );

    $opts['fdd']['body'] = array(
        'name'     => 'Message',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true
    );

    $opts['fdd']['destination'] = array(
        'name'     => 'Destination',
        'select'   => 'D'
    );

    $opts['fdd']['destination']['values2'] = array(
        '/topics/general' => 'General',
        '/topics/android' => 'Android',
        '/topics/ios' => 'iOS'
    );

    $date = new DateTime();
    $date = date('d/m/Y H:i:s');

    $opts['fdd']['datetime'] = array(
        'name'     => 'Date de création',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true,
        'default'  => $date,
        'options'  => 'LAFVCPDR'
    );

    $opts['options'] = 'A';
?>

<div id="middle">
    <div id="left-column">
        <h3>Notifications</h3>
        <ul class="nav">
            <li>Permet d'envoyer des notifications</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Notifications</h1>
        </div>
        
        <div class="table">
            <?php
            // call to phpMyEdit
            require_once 'phpMyEdit.class.php';
            new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>