<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'pages';
    // Champ clé unique
    $opts['key'] = 'id_pages';
    // Tri par défaut
    $opts['sort_field'] = array('id_pages');
    // Boutons navigation
    $opts['buttons']['L']['up'] = array('add');

    // Champs de la table
    $opts['fdd']['id_pages'] = array(
      'name'     => 'ID',
      'select'   => 'T',
      'options'  => 'LFR', // auto increment
      'maxlen'   => 11,
      'default'  => '0',
      'sort'     => true
    );

    $opts['fdd']['id_pages']['css'] = array('postfix' => 'ColId');

    $opts['fdd']['titre_pages'] = array(
      'name'     => 'Titre',
      'select'   => 'T',
      'options'  => 'ACPDVLF',
      'maxlen'   => 255,
      'sort'     => true
    );
    $opts['fdd']['contenu_pages'] = array(
      'name'     => 'Contenu',
      'select'   => 'T',
      'options'  => 'ACPDV',
      'maxlen'   => 65535,
      'textarea' => array(
        'rows' => 5,
        'cols' => 50)
    );
    $opts['fdd']['nom_fichier_pages'] = array(
      'name'     => 'Nom du fichier html',
      'select'   => 'T',
      'options'  => 'ACPDVLF',
      'maxlen'   => 255,
      'sort'     => true
    );
    $opts['fdd']['contenu_pages']['escape'] = false;

    $opts['fdd']['dataset'] = array(
        'name'     => 'Dataset',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true,
        'default' => 1
    );

    // On selectionne tous les datasets
    $items = $ezdb->get_results('SELECT * FROM datasets');

    $datasetsAll = array();

    foreach($items as $item) 
    { 
        $datasetsAll[$item->version] = $item->version;
    }

    $opts['fdd']['dataset']['values2'] = $datasetsAll;

    $opts['fdd']['action'] = array(
        'name'     => 'Action',
        'select'   => 'D',
        'sort'     => true
    );

    $opts['fdd']['action']['values2'] = array(
        'ADD' => 'Ajouter',
        'DEL' => 'Supprimer',
        'UP' => 'Modifier'
    ); 
?>

<script type="text/javascript">

    $(function() {
        var config = {
            toolbar:   'ToolbarUrgences'
        };

        // Initialize the editor.
        // Callback function can be passed and executed after full instance creation.
        $("textarea[name='PME_data_contenu_pages']").ckeditor(config);
    });
</script>

<div id="middle">
    <div id="left-column">
        <h3>Pages</h3>
        <ul class="nav">
            <li>Permet de saisir le contenu des pages de l'application</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Pages</h1>
        </div>
        <div class="table">
            <?php
                // Call to phpMyEdit
                require_once 'phpMyEdit.class.php';
                new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>