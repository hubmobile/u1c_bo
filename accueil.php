<?php
    include("includes/config.php");
    include("includes/header.php");
?>

<div id="middle">
    <div id="left-column">
        <h3>Urgences en un clic</h3>

        <ul class="nav">
            <li>Sélectionner ci-dessus une rubrique de travail</li>
        </ul>
    </div>

    <div id="center-column">
        <div class="top-bar">
            <h1>Urgences en un clic</h1>
        </div>

        <div class="table">
            <p style="text-align:center;"><img style="max-width: 100%;" src="img/image_presentation.png" alt="" /></p>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>

