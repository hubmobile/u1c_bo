<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'datasets';
    // Champ clé unique
    $opts['key'] = 'id_datasets';
    // Tri par défaut
    $opts['sort_field'] = array('id_datasets');
    // boutons navigation
    $opts['buttons']['L']['up'] = array('add');

    //champs de la table
    $opts['fdd']['id_datasets'] = array(
        'name'     => 'ID',
        'select'   => 'T',
        'options'  => 'VCPDR', // auto increment
        'maxlen'   => 11,
        'default'  => '0',
        'sort'     => true
    );
    $opts['fdd']['version'] = array(
        'name'     => 'Version',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true
    );

    $opts['fdd']['libelle'] = array(
        'name'     => 'Libellé',
        'select'   => 'T',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 120)
    );

    // Save version courante dataset
    if(isset($_POST['select_dataset']))
    {   
        $ezdb->query('UPDATE datasets SET actif = 0');
        $ezdb->query('UPDATE datasets SET actif = 1 WHERE version = '.$_POST["select_dataset"].'');
    }
?>

<div id="middle">
    <div id="left-column">
        <h3>Datasets</h3>
        <ul class="nav">
            <li>Permet de créer des nouvelles versions de données</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Datasets</h1>
        </div>
        
        <form action="#" method="post">
            <h2 style="float: left; margin: 10px;">Dataset courant : </h2>
            <select name="select_dataset" style="float: left; margin: 10px;" >
                <?php 
                    // On selectionne tous les datasets
                    $items = $ezdb->get_results('SELECT * FROM datasets ORDER BY version ASC');

                    foreach($items as $item) 
                    { 
                        ?>
                        <option <?php if ($item->actif == 1 ) echo 'selected'; ?> value="<?php echo $item->version; ?>"><?php echo $item->version; ?></option>
                        <?php
                    }
                ?>
            </select>

            <input type="submit" name="exporter" id="exporter" class="pme-add" style="display:inline; margin: 9px;" value="Enregistrer" />
        </form>
        
        <div class="table">
            <?php
            // call to phpMyEdit
            require_once 'phpMyEdit.class.php';
            new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>