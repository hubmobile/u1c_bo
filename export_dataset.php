<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");
    include("includes/fonctions.php");

    function mycopy($s1,$s2) 
    {
        $path = pathinfo($s2);
        if (!file_exists($path['dirname']))
        {
            mkdir($path['dirname'], 0777, true);
        }   
    
        if (!copy($s1,$s2)) 
        {
            echo "Copy failed \n";
        }
    }

    function forceFilePutContents($filepath, $message)
    {
        try 
        {
            $isInFolder = preg_match("/^(.*)\/([^\/]+)$/", $filepath, $filepathMatches);
            
            if($isInFolder) 
            {
                $folderName = $filepathMatches[1];
                $fileName = $filepathMatches[2];
                
                if (!is_dir($folderName))
                    mkdir($folderName, 0777, true);
            }
            file_put_contents($filepath, $message);
        } 
        catch (Exception $e) 
        {
            echo "ERR: error writing '$message' to '$filepath', ". $e->getMessage();
        }
    }
?>

<div id="middle">
    <div id="left-column">
        <h3>Exportation dataset</h3>
        <ul class="nav">
            <li>Cette page permet de générer les données du dataset précédemment sélectionné (<?php echo $_POST['select_dataset']; ?>). La version des données sera automatiquement disponible dans l'application mobile. Aucun téléchargement ne sera nécessaire à la fin de l'éxécution de ce script.</li>
        </ul>
    </div>
    
    <div id="center-column">
        <div class="table">

            <div class="top-bar">
                <h1>Exportation du dataset <?php echo $_POST['select_dataset']; ?></h1>
                <p style="padding: 10px;"><b>Attention :</b> cette rubrique est réservée aux administrateurs</p>
            </div>

            <div style="clear:both;">&nbsp;</div>
            
            <div class="logs_export">
            
                <?php
                    echo "<h2>Logs</h2></br></br>";
                
                    $specialites = array(
                        '1' => 'A',
                        '2' => 'B',
                        '3' => 'C',
                        '4' => 'D',
                        '5' => 'E',
                        '6' => 'F',
                        '7' => 'G',
                        '8' => 'H',
                        '9' => 'I',
                        '10' => 'J',
                        '11' => 'K',
                        '12' => 'L',
                        '13' => 'M',
                        '14' => 'N',
                        '15' => 'O',
                        '16' => 'P',
                        '17' => 'Q',
                        '18' => 'R',
                        '19' => 'S',
                        '20' => 'T',
                        '21' => 'U',
                        '22' => 'V',
                        '23' => 'W',
                        '24' => 'X',
                        '25' => 'Y',
                        '26' => 'Z'
                    );

                    // Init array global
                    $arrayJSON = array();

                    // PATHOLOGIES JSON
                    echo "<h2>Traitement des <b>pathologies</b></h2>";
                    // Init array pathologies
                    $arrayPathologies = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM pathologies WHERE titre_pathologies REGEXP "^'.$value.'" AND dataset = '.$_POST["select_dataset"].' ORDER BY titre_pathologies ASC');

                        // Init array de fiches
                        $fiches = array();
                        
                        if(isset($items))
                        {
                            echo "<p>Traitement des fiches de la <b>lettre $value</b></p>";

                            foreach($items as $item)
                            {
                                echo "<li>Fiche n°$item->id_pathologies : $item->titre_pathologies</li>";

                                // Création array chapters et vérification des chapitres
                                $chapters = array();

                                if($item->pi_pathologies!="") array_push($chapters, "PI");
                                if($item->c_pathologies!="") array_push($chapters, "C");
                                if($item->sp_pathologies!="") array_push($chapters, "SP");
                                if($item->de_pathologies!="") array_push($chapters, "DE");
                                if($item->dd_pathologies!="") array_push($chapters, "DD");
                                if($item->t_pathologies!="") array_push($chapters, "T");
                                if($item->s_pathologies!="") array_push($chapters, "S");
                                if($item->d_pathologies!="") array_push($chapters, "D");
                                if($item->md_pathologies!="") array_push($chapters, "MD");
                                if($item->a_pathologies!="") array_push($chapters, "A");
                                if($item->b_pathologies!="") array_push($chapters, "B");

                                // Initialisation array fiche
                                $fiche = array(
                                    'title' => $item->titre_pathologies,
                                    'file' => "pathologies_".$item->id_pathologies.".html",
                                    'coverage' => $item->cible_pathologies,
                                    'keywords' => $item->r_pathologies,
                                    'authors' => $item->auteur_pathologies,
                                    'chapters' => $chapters,
                                    'speciality' => $item->specialite_pathologies,
                                    'action' => $item->action
                                );

                                // On boucle sur les médias de cette fiche
                                $mediasAll = $ezdb->get_results('SELECT * FROM medias WHERE pathologie_medias REGEXP "(^'.$item->id_pathologies.'$)|(^'.$item->id_pathologies.',)|(,'.$item->id_pathologies.',)|(,'.$item->id_pathologies.'$)" ORDER BY ordre_medias ASC');

                                if(isset($mediasAll))
                                {
                                    // Création array medias
                                    $medias = array();

                                    foreach($mediasAll as $media)
                                    {
                                        echo "<ul><li>Media ".$media->id_medias." >> ".$media->nom_medias."</li></ul>";

                                        $mediaArray = array(
                                            'title' => $media->nom_medias,
                                            'subtitle' => $media->legende_medias,
                                            'type' => $media->type_medias,
                                            'thumb' => $media->vignette_medias,
                                            'file' => $media->fichier_medias
                                        );

                                        array_push($medias, $mediaArray);
                                    }

                                    $fiche['medias'] = $medias;
                                }

                                array_push($fiches, $fiche);
                            }

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de fiche pour la <b>lettre $value</b></p>";
                        }
                        
                        $arrayPathologies[$value] = $fiches;
                    }

                    $arrayJSON["pathologies"] = $arrayPathologies;
                    // FIN PATHOLOGIES JSON
                
                
                    // TECHNIQUES JSON
                    echo "<h2>Traitement des <b>techniques</b></h2>";
                    // Init array techniques
                    $arrayTechniques = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM techniques WHERE titre_techniques REGEXP "^'.$value.'" AND dataset = '.$_POST["select_dataset"].' ORDER BY titre_techniques ASC');

                        // Init array de fiches
                        $fiches = array();
                        
                        if(isset($items))
                        {
                            echo "<p>Traitement des fiches de la <b>lettre $value</b></p>";

                            foreach($items as $item)
                            {
                                $trans = array("&#39;" => "'","&OElig;" => "oe","&oelig;" => "oe");

                                echo "<li>Fiche n°$item->id_techniques : $item->titre_techniques</li>";
                                
                                // Création array chapters et vérification des chapitres
                                $chapters = array();
                                
                                if($item->pi_techniques!="") array_push($chapters, "PI");
                                if($item->i_techniques!="") array_push($chapters, "I");;
                                if($item->ci_techniques!="") array_push($chapters, "CI");
                                if($item->pm_techniques!="") array_push($chapters, "PM");
                                if($item->dt_techniques!="") array_push($chapters, "DT");
                                if($item->pe_techniques!="") array_push($chapters, "PE");
                                if($item->p_techniques!="") array_push($chapters, "P");
                                if($item->c_techniques!="") array_push($chapters, "C");
                                if($item->s_techniques!="") array_push($chapters, "S");
                                if($item->b_techniques!="") array_push($chapters, "B");

                                // Initialisation array fiche
                                $fiche = array(
                                    'title' => $item->titre_techniques,
                                    'file' => "techniques_".$item->id_techniques.".html",
                                    'coverage' => $item->cible_techniques,
                                    'keywords' => $item->r_techniques,
                                    'authors' => $item->auteur_techniques,
                                    'chapters' => $chapters,
                                    'speciality' => $item->specialite_techniques,
                                    'action' => $item->action
                                );

                                // On boucle sur les médias de cette fiche
                                $mediasAll = $ezdb->get_results('SELECT * FROM medias WHERE technique_medias REGEXP "(^'.$item->id_techniques.'$)|(^'.$item->id_techniques.',)|(,'.$item->id_techniques.',)|(,'.$item->id_techniques.'$)" ORDER BY ordre_medias ASC');

                                if(isset($mediasAll))
                                {
                                    // Création array medias
                                    $medias = array();

                                    foreach($mediasAll as $media)
                                    {
                                        echo "<ul><li>Media ".$media->id_medias." >> ".$media->nom_medias."</li></ul>";

                                        $mediaArray = array(
                                            'title' => $media->nom_medias,
                                            'subtitle' => $media->legende_medias,
                                            'type' => $media->type_medias,
                                            'thumb' => $media->vignette_medias,
                                            'file' => $media->fichier_medias
                                        );

                                        array_push($medias, $mediaArray);
                                    }

                                    $fiche['medias'] = $medias;
                                }

                                array_push($fiches, $fiche);
                            }

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de fiche pour la <b>lettre $value</b></p>";
                        }
                        
                        $arrayTechniques[$value] = $fiches;
                    }

                    $arrayJSON["techniques"] = $arrayTechniques;
                    // FIN TECHNIQUES JSON
                
                
                    // FICHES IDE JSON
                    echo "<h2>Traitement des <b>fiches ide</b></h2>";
                    // Init array fiches ide
                    $arrayFichesIDE = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM fiches_ide WHERE titre_fiche_ide REGEXP "^'.$value.'" AND dataset = '.$_POST["select_dataset"].' ORDER BY titre_fiche_ide ASC');

                        // Init array de fiches
                        $fiches = array();
                        
                        if(isset($items))
                        {
                            echo "<p>Traitement des fiches de la <b>lettre $value</b></p>";

                            foreach($items as $item)
                            {
                                $trans = array("&#39;" => "'","&OElig;" => "oe","&oelig;" => "oe");

                                echo "<li>Fiche n°$item->id_fiche_ide : $item->titre_fiche_ide, $trans</li>";
                                
                                // Initialisation array fiche
                                $fiche = array(
                                    'title' => $item->titre_fiche_ide, $trans,
                                    'file' => "fiches-ide_".$item->id_fiche_ide.".html",
                                    'action' => $item->action
                                );
                                
                                array_push($fiches, $fiche);
                            }

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de fiche pour la <b>lettre $value</b></p>";
                        }
                        
                        $arrayFichesIDE[$value] = $fiches;
                    }

                    $arrayJSON["fiches-ide"] = $arrayFichesIDE;
                    // FIN FICHES IDE JSON
                
                
                    // MEDIAS JSON
                    echo "<h2>Traitement des <b>medias</b></h2>";
                    // Init array medias
                    $arrayMedias = array();

                    foreach($specialites as $value)
                    {
                        // On selectionne toutes les fiches appartenant à la spécialité
                        $items = $ezdb->get_results('SELECT * FROM medias WHERE nom_medias REGEXP "^'.$value.'" AND dataset = '.$_POST["select_dataset"].' ORDER BY nom_medias ASC');

                        // Init array de fiches
                        $fiches = array();
                        
                        if(isset($items))
                        {
                            echo "<p>Traitement des medias de la <b>lettre $value</b></p>";

                            foreach($items as $item)
                            {
                                // On récupère toutes les fiches pathologies dans lesquels ce media apparait
                                $listesfichesPathologies = "";
                                
                                // On selectionne toutes les fiches pour lesquel ce media apparait dans au moins un paragraphe
                                $fichesReq = $ezdb->get_results("
                                SELECT id_pathologies FROM pathologies
                                WHERE pi_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR c_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR sp_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR de_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR dd_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR t_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR s_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR d_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR md_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                 OR a_pathologies LIKE '%MEDIA_".$item->id_medias."%'
                                ");
                                
                                if(isset($fichesReq))
                                {
                                    foreach($fichesReq as $ficheReq)
                                    {
                                        $listesfichesPathologies .= "pathologies_".$ficheReq->id_pathologies.".html,";
                                    }
                                }
                                else
                                {
                                    $listesfichesPathologies = "";
                                }
                                
                                // On récupère toutes les fiches technologies dans lesquels ce media apparait
                                $listesfichesTechniques = "";
                                
                                // On selectionne toutes les fiches pour lesquel ce media apparait dans au moins un paragraphe
                                $fichesTech = $ezdb->get_results("
                                SELECT id_techniques FROM techniques
                                    WHERE pi_techniques LIKE '%MEDIA_".$item->id_medias."'
                                    OR i_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR ci_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR pm_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR dt_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR pe_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR p_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR c_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                    OR s_techniques LIKE '%MEDIA_".$item->id_medias."%'
                                ");
                                
                                if(isset($fichesTech))
                                {
                                    foreach($fichesTech as $ficheTech)
                                    {
                                        $listesfichesTechniques .= "techniques_".$ficheTech->id_techniques.".html,";
                                    }
                                }
                                else
                                {
                                    $listesfichesTechniques = "";
                                }
                                
                                $trans = array(
                                    "&#39;" => "'",
                                    "&OElig;" => "oe",
                                    "&oelig;" => "oe"
                                );
                                
                                $extensionMedia = str_replace('.','',strrchr($item->fichier_medias, '.'));

                                switch($extensionMedia)
                                {
                                    Case "mp4":
                                    Case "mov":
                                        $fondMedia = "storebgvideo.png";
                                        $protocoleMedia = "vidp://";
                                        break;
                                    Case "png":
                                    Case "jpg":
                                    Case "jpeg":
                                        $fondMedia = "storebgphoto.png";
                                        $protocoleMedia = "imgp://";
                                        break;
                                    Case "pdf":
                                        $fondMedia = "storebgdoc.png";
                                        $protocoleMedia = "pdfp://";
                                        break;
                                    default:
                                        $fondMedia = "storebgblank.png";
                                        $protocoleMedia = "http://";
                                        break;
                                }
                                
                                if($item->vignette_medias == "")
                                {
                                    $vignetteMedia = $fondMedia; 
                                }
                                else
                                { 
                                    $vignetteMedia = $item->vignette_medias;
                                }
                                
                                echo "<li>Media n°$item->id_medias : $item->nom_medias<br />Pathologies : ".$listesfichesPathologies."<br />Techniques : ".$listesfichesTechniques."</li>";
                                
                                // Initialisation array media
                                $fiche = array(
                                    'title' => $item->nom_medias,
                                    'subtitle' => $item->legende_medias,
                                    'speciality' => $item->specialite_medias,
                                    'type' => $item->type_medias,
                                    'protocol' => $protocoleMedia,
                                    'thumb' => $vignetteMedia,
                                    'file' => $item->fichier_medias,
                                    'pathologies' => $listesfichesPathologies,
                                    'techniques' => $listesfichesTechniques,
                                    'keywords' => $item->tags_medias,
                                    'action' => $item->action
                                );

                                array_push($fiches, $fiche);
                            }

                            echo "</ul><p>- - - fin de section</p>";
                        }
                        else
                        {
                            echo "<p>Pas de media pour la <b>lettre $value</b></p>";
                        }
                        
                        $arrayMedias[$value] = $fiches;
                    }

                    $arrayJSON["medias"] = $arrayMedias;
                    // FIN MEDIAS JSON
                
                
                    // GENERATION DES FICHIERS PATHOLOGIES
                    $fiches = $ezdb->get_results('SELECT * FROM pathologies WHERE dataset = '.$_POST["select_dataset"].' AND action != "DEL" ORDER BY id_pathologies ASC');
                    
                    if (!empty($fiches))
                    {
                        foreach($fiches as $fiche)
                        {
                            $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.$fiche->titre_pathologies.'</title></head><body><div id="content"><div class="entete"><b>'.id2champ('cibles','label_cibles','id_cibles',$fiche->cible_pathologies).'</b><br />Spécialité : '.ligneSpecialites($fiche->specialite_pathologies).'</div><div class="contenu">';

                            if($fiche->pi_pathologies!="")
                            {
                                $contenu .= '<h1><a name="PI"></a>Points importants</h1>'.boutonMedia($fiche->pi_pathologies);
                            }

                            if($fiche->c_pathologies!="")
                            {
                                $contenu .= '<h1><a name="C"></a>Présentation clinique / CIMU</h1>'.boutonMedia($fiche->c_pathologies);
                            }

                            if($fiche->sp_pathologies!="")
                            {
                                $contenu .= '<h1><a name="SP"></a>Signes paracliniques</h1>'.boutonMedia($fiche->sp_pathologies);
                            }

                            if($fiche->de_pathologies!="")
                            {
                                $contenu .= '<h1><a name="DE"></a>Diagnostic étiologique</h1>'.boutonMedia($fiche->de_pathologies);
                            }

                            if($fiche->dd_pathologies!="")
                            {
                                $contenu .= '<h1><a name="DD"></a>Diagnostic différentiel</h1>'.boutonMedia($fiche->dd_pathologies);
                            }

                            if($fiche->t_pathologies!="")
                            {
                                $contenu .= '<h1><a name="T"></a>Traitement</h1>'.boutonMedia($fiche->t_pathologies);
                            }

                            if($fiche->s_pathologies!="")
                            {
                                $contenu .= '<h1><a name="S"></a>Surveillance</h1>'.boutonMedia($fiche->s_pathologies);
                            }

                            if($fiche->d_pathologies!="")
                            {
                                $contenu .= '<h1><a name="D"></a>Devenir / orientation</h1>'.boutonMedia($fiche->d_pathologies);
                            }

                            if($fiche->md_pathologies!="")
                            {
                                $contenu .= '<h1><a name="MD"></a>Mécanisme / description</h1>'.boutonMedia($fiche->md_pathologies);
                            }

                            if($fiche->a_pathologies!="")
                            {
                                $contenu .= '<h1><a name="A"></a>Algorithme</h1>'.boutonMedia($fiche->a_pathologies);
                            }

                            if($fiche->b_pathologies!="")
                            {
                                $contenu .= '<h1><a name="B"></a>Bibliographie</h1>'.boutonMedia($fiche->b_pathologies);
                            }

                            $contenu .= '<p style="font-weight: bold;">Auteur(s) : '.$fiche->auteur_pathologies.'</p></div></div></body></html>';

                            // On ecrit le fichier html
                            forceFilePutContents("ressources/".$_POST['select_dataset']."/pathologies/pathologies_".$fiche->id_pathologies.".html", $contenu);

                            echo "<p>Génération de la <b>fiche n°".$fiche->id_pathologies."</b> avec succès</p>";
                        }
                    }
                    // FIN DE BOUCLE FICHIERS PATHOLOGIES
                        
                
                    // GENERATION DES FICHIERS TECHNIQUES
                    $fiches = $ezdb->get_results('SELECT * FROM techniques WHERE dataset = '.$_POST["select_dataset"].' AND action != "DEL" ORDER BY id_techniques ASC');
                    
                    if (!empty($fiches))
                    {
                        foreach($fiches as $fiche)
                        {
                            $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.$fiche->titre_techniques.'</title></head><body><div id="content"><div class="entete"><b>'.id2champ('cibles','label_cibles','id_cibles',$fiche->cible_techniques).'</b><br />Spécialité : '.ligneSpecialites($fiche->specialite_techniques).'</div><div class="contenu">';

                            if($fiche->pi_techniques!="")
                            {
                                $contenu .= '<h1><a name="PI"></a>Points importants</h1>'.boutonMedia($fiche->pi_techniques);
                            }

                            if($fiche->i_techniques!="")
                            {
                                $contenu .= '<h1><a name="I"></a>Indications</h1>'.boutonMedia($fiche->i_techniques);
                            }

                            if($fiche->ci_techniques!="")
                            {
                                $contenu .= '<h1><a name="CI"></a>Contre-indications</h1>'.boutonMedia($fiche->ci_techniques);
                            }

                            if($fiche->pm_techniques!="")
                            {
                                $contenu .= '<h1><a name="PM"></a>Présentation du matériel</h1>'.boutonMedia($fiche->pm_techniques);
                            }

                            if($fiche->dt_techniques!="")
                            {
                                $contenu .= '<h1><a name="DT"></a>Description de la technique</h1>'.boutonMedia($fiche->dt_techniques);
                            }

                            if($fiche->pe_techniques!="")
                            {
                                $contenu .= '<h1><a name="PE"></a>Précautions d’emploi</h1>'.boutonMedia($fiche->pe_techniques);
                            }

                            if($fiche->p_techniques!="")
                            {
                                $contenu .= '<h1><a name="P"></a>Pièges éventuels</h1>'.boutonMedia($fiche->p_techniques);
                            }

                            if($fiche->c_techniques!="")
                            {
                                $contenu .= '<h1><a name="C"></a>Complications</h1>'.boutonMedia($fiche->c_techniques);
                            }

                            if($fiche->s_techniques!="")
                            {
                                $contenu .= '<h1><a name="S"></a>Surveillance</h1>'.boutonMedia($fiche->s_techniques);
                            }

                            if($fiche->b_techniques!="")
                            {
                                $contenu .= '<h1><a name="B"></a>Bibliographie</h1>'.$fiche->b_techniques;
                            }

                            $contenu .= '<p style="font-weight: bold;">Auteur(s) : '.$fiche->auteur_techniques.'</p></div></div></body></html>';

                            // On ecrit le fichier html
                            forceFilePutContents("ressources/".$_POST['select_dataset']."/techniques/techniques_".$fiche->id_techniques.".html", $contenu);

                            echo "<p>Génération de la <b>fiche n°".$fiche->id_techniques."</b> avec succès</p>";
                        } 
                    } 
                    // FIN DE BOUCLE FICHIERS TECHNIQUES
                
                
                    // GENERATION DES FICHIERS FICHES IDE
                    $fiches = $ezdb->get_results('SELECT * FROM fiches_ide WHERE dataset = '.$_POST["select_dataset"].' AND action != "DEL" ORDER BY id_fiche_ide ASC');
                    
                    if (!empty($fiches))
                    {
                        foreach($fiches as $fiche)
                        {
                            $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.5, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.utf8_decode($fiche->titre_fiche_ide).'</title></head><body class="fiche-ide"><div id="content"><div class="entete">'.utf8_decode($fiche->titre_fiche_ide).'</div><div class="contenu">';

                            $contenu .= $fiche->contenu_fiche_ide;

                            $contenu .= '</div></div></body></html>';

                            // On ecrit le fichier html
                            forceFilePutContents("ressources/".$_POST['select_dataset']."/fiches-ide/fiches-ide_".$fiche->id_fiche_ide.".html", $contenu);

                            echo "<p>Génération de la <b>fiche n°".$fiche->id_fiche_ide."</b> avec succès</p>";
                        }
                    }
                    // FIN DE BOUCLE FICHIERS FICHES IDE
                
                
                    // COPIE DES FICHIERS MEDIAS (FICHIERS ET VIGNETTES)
                    if (!empty($arrayJSON["medias"]))
                    {
                        foreach($arrayJSON["medias"] as $lettre)
                        {
                            foreach($lettre as $mediasInLettre)
                            {
                                if ($mediasInLettre['action'] != 'DEL')
                                {
                                    // Copie de la vignette et du fichier dans le directory des ressources dataset
                                    $src = 'medias/vignettes/'.$mediasInLettre['thumb'];
                                    $dst = 'ressources/'.$_POST['select_dataset'].'/vignettes/'.$mediasInLettre['thumb'];
                                    mycopy($src, $dst);

                                    $src = 'medias/fichiers/'.$mediasInLettre['file'];
                                    $dst = 'ressources/'.$_POST['select_dataset'].'/fichiers/'.$mediasInLettre['file'];
                                    mycopy($src, $dst);
                                }
                            }
                        }
                    }
                    // FIN COPIE DES FICHIERS MEDIAS (FICHIERS ET VIGNETTES)
                
                
                    // GENERATION FICHIER AUTEURS.JSON
                    $ressourcesAuteur = $ezdb->get_results('SELECT * FROM ressources WHERE fichier_ressources = "auteurs.json" AND action != "DEL" AND dataset = '.$_POST["select_dataset"].'');
                
                    if (!empty($ressourcesAuteur))
                    {
                        foreach($ressourcesAuteur as $ressource)
                        {
                            if ($ressource->dataset == $_POST['select_dataset'])
                            {
                                $items = $ezdb->get_results('SELECT * FROM auteurs');

                                // Init array
                                $auteurs = array();

                                if(isset($items))
                                {
                                    $array = array();

                                    foreach($items as $item)
                                    {
                                        $trans = array("&#39;" => "'","&OElig;" => "oe","&oelig;" => "oe");

                                        // Initialisation array fiche
                                        $auteur = array(
                                            'name' => $item->nom_auteurs,
                                            'locality' => $item->lieu_auteurs
                                        );

                                        array_push($array, $auteur);
                                    }

                                    echo "</ul><p>- - - fin de section</p>";
                                }
                                else
                                {
                                    echo "<p>Pas d'auteur</p>";
                                }

                                $auteurs['auteurs'] = $array;

                                // On ecrit le fichier json
                                forceFilePutContents("ressources_bo/auteurs.json", json_encode($auteurs));

                                echo "<p>Fichier <b>auteurs.json</b> généré avec succès.</p> ";
                            }
                        }
                        // FIN GENERATION FICHIER AUTEURS.JSON
                    }
                    
                
                    // GENERATION FICHIER COMITES.JSON
                    $ressourcesComites = $ezdb->get_results('SELECT * FROM ressources WHERE fichier_ressources = "comites.json" AND action != "DEL" AND dataset = '.$_POST["select_dataset"].'');
                    
                    if (!empty($ressourcesComites))
                    {
                        foreach($ressourcesComites as $ressource)
                        {
                            if ($ressource->dataset == $_POST['select_dataset'])
                            {
                                $items = $ezdb->get_results('SELECT * FROM comites');

                                // Init array
                                $comites = array();

                                if(isset($items))
                                {
                                    $array = array();

                                    foreach($items as $item)
                                    {
                                        $trans = array("&#39;" => "'","&OElig;" => "oe","&oelig;" => "oe");

                                        // Initialisation array comite
                                        $comite = array(
                                            'name' => $item->nom_comites,
                                            'locality' => $item->lieu_comites
                                        );

                                        array_push($array, $comite);
                                    }

                                    echo "</ul><p>- - - fin de section</p>";
                                }
                                else
                                {
                                    echo "<p>Pas de comité de relecture</p>";
                                }

                                $comites['comites'] = $array;

                                // On ecrit le fichier json
                                forceFilePutContents("ressources_bo/comites.json", json_encode($comites));

                                echo "<p>Fichier <b>comites.json</b> généré avec succès.</p> ";
                            }
                        }
                        // FIN GENERATION FICHIER COMITES.JSON
                    }
                    
                
                    // GENERATION DES PAGES A PROPOS
                    $pages = $ezdb->get_results('SELECT * FROM pages WHERE action != "DEL" AND dataset = '.$_POST["select_dataset"].'');
                
                    if (!empty($pages))
                    {
                        foreach($pages as $page)
                        {
                            $contenu = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="yes" name="apple-mobile-web-app-capable" /><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /><meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" /><link href="style.css" rel="stylesheet" type="text/css" /><link href="view.css" rel="stylesheet" type="text/css" /><title>'.utf8_decode($page->titre_pages).'</title></head><body><div class="contenu">';

                            $contenu .= $page->contenu_pages;

                            $contenu .= '</div></body></html>';

                            // On ecrit le fichier html
                            forceFilePutContents("ressources_bo/".$page->nom_fichier_pages, $contenu);

                            echo "<p>Génération du fichier <b>".$page->titre_pages."</b> avec succès</p>";
                        } 
                    }
                    // FIN GENERATION DES PAGES A PROPOS
                
                
                    // RESSOURCES
                    $ressourcesBDDataset = $ezdb->get_results('SELECT * FROM ressources WHERE dataset = '.$_POST["select_dataset"].'');

                    // Init array de fiches
                    $ressources = array();

                    if (!empty($ressourcesBDDataset))
                    {
                        foreach($ressourcesBDDataset as $ressourceBD)
                        {
                            // Initialisation array fiche
                            $ressource = array(
                                'action' => $ressourceBD->action,
                                'file' => $ressourceBD->fichier_ressources
                            );

                            array_push($ressources, $ressource);
                        }
                    }
                
                    $ressourcesPagesBDDataset = $ezdb->get_results('SELECT * FROM pages WHERE dataset = '.$_POST["select_dataset"].'');

                    if (!empty($ressourcesPagesBDDataset))
                    {
                        foreach($ressourcesPagesBDDataset as $ressourcePagesBD)
                        {
                            // Initialisation array fiche
                            $ressource = array(
                                'action' => $ressourcePagesBD->action,
                                'file' => $ressourcePagesBD->nom_fichier_pages
                            );

                            array_push($ressources, $ressource);
                        }
                    }
                
                    $arrayJSON["ressources"] = $ressources;
                   
                    // COPIE DES RESSOURCES
                    if (!empty($arrayJSON["ressources"]))
                    {
                        foreach($arrayJSON["ressources"] as $res)
                        { 
                            if ($res['action'] != 'DEL')
                            {
                                // Copie de la ressource dans le directory des ressources dataset
                                $src = 'ressources_bo/'.$res['file'];
                                $dst = 'ressources/'.$_POST['select_dataset'].'/ressources/'.$res['file'];
                                mycopy($src, $dst);
                            } 
                        }
                    }
                    // FIN COPIE DES RESSOURCES
                
                    try 
                    {
                        // On ecrit le fichier json
                        forceFilePutContents("ressources/".$_POST['select_dataset']."/".$_POST['select_dataset'].".json", json_encode($arrayJSON));

                        echo "<p>Fichier <b>".$_POST['select_dataset'].".json</b> généré avec succès.</p>";
                    } 
                    catch (Exception $e) 
                    {
                        echo 'Erreur lors de la création du json : ',  $e->getMessage(), "\n";
                    }
                
                    echo "<h2>FIN DU SCRIPT</h2>";
                ?>
            </div>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>