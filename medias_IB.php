<?php
    // On test s'il y a un fichier à uploader par la présence de son nom
    if($_FILES['PME_data_fichier_medias_fichier']['name']!="")
    {
        echo "<p class='msg_vert'>Il y a un fichier à charger : ".$_FILES['PME_data_fichier_medias_fichier']['name']."</p>";
    }
    else
    {
        echo "<p class='msg_jaune'>Il n'y a pas de fichier à charger : ".$_FILES['PME_data_fichier_medias_fichier']['name']."</p>";
    }

    // On test s'il y a une vignette à uploader par la présence de son nom
    if($_FILES['PME_data_vignette_medias_fichier']['name']!="")
    {
        echo "<p class='msg_vert'>Il y a une vignette à charger : ".$_FILES['PME_data_vignette_medias_fichier']['name']."</p>";
    }
    else
    {
        echo "<p class='msg_jaune'>Il n'y a pas de vignette à charger : ".$_FILES['PME_data_vignette_medias_fichier']['name']."</p>";
    }

    // Pour le fichier
    if($_FILES['PME_data_fichier_medias_fichier']['name'] !="")
    {
        $fichierOrigine = $_FILES['PME_data_fichier_medias_fichier']['name'];
        $fichierChemin = pathinfo($fichierOrigine);
        $fichierExtension = $fichierChemin['extension'];
        $fichierAutorisees = array("jpeg","jpg","mov","mp4","png","pdf");
        $fichierDestination = dirname(__FILE__)."/medias/fichiers/";
        
        // Traitement du fichier 
        // On récupère l'ID du média concerné avec $this->rec pour fabriquer le nom du fichier
        if (!(in_array($fichierExtension, $fichierAutorisees))) 
        {
            echo "<p class='msg_rouge'>Le fichier ".$_FILES['PME_data_fichier_medias_fichier']['name']." n'a pas l'extension attendue</p>";
        } 
        else 
        {
            $result = $this->myquery("SELECT AUTO_INCREMENT as 'count' FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'app_urgences' AND TABLE_NAME = 'medias'");
            $rows = $result->fetch_object();
            $count = $rows->count;

            $fichierNom = "fichier_".$count.".".$fichierExtension;

            if (move_uploaded_file($_FILES["PME_data_fichier_medias_fichier"]["tmp_name"],$fichierDestination.$fichierNom)) 
            {
                echo "<p class='msg_vert'>Le fichier temporaire ".$_FILES["PME_data_fichier_medias_fichier"]["tmp_name"]." a été déplacé vers ".$fichierDestination.$fichierNom."</p>";
            } 
            else
            {
                echo "<p class='msg_jaune'>Le fichier n'a pas été uploadé car il est trop gros.</p>";
            }
        }
        
        // On affecte le nom du fichier
        $newvals['fichier_medias'] = $fichierNom;
    }

    // pour la vignette
    if($_FILES['PME_data_vignette_medias_fichier']['name'] != "")
    {
        $vignetteOrigine = $_FILES['PME_data_vignette_medias_fichier']['name'];
        $vignetteChemin = pathinfo($vignetteOrigine);
        $vignetteExtension = $vignetteChemin['extension'];
        $vignetteAutorisees = array("jpeg","jpg","png");
        $vignetteDestination = dirname(__FILE__)."/medias/vignettes/";
        
        // Traitement du vignette 
        // On récupère l'ID du média concerné avec $this->rec pour fabriquer le nom du vignette
        if (!(in_array($vignetteExtension, $vignetteAutorisees))) 
        {
            echo "<p class='msg_rouge'>La vignette ".$_FILES['PME_data_vignette_medias_fichier']['name']." n'a pas l'extension attendue</p>";
        } 
        else 
        {
            $result = $this->myquery("SELECT AUTO_INCREMENT as 'count' FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'app_urgences' AND TABLE_NAME = 'medias'");
            $rows = $result->fetch_object();
            $count = $rows->count;

            $vignetteNom = "vignette_".$count.".".$vignetteExtension;

            if (move_uploaded_file($_FILES["PME_data_vignette_medias_fichier"]["tmp_name"],$vignetteDestination.$vignetteNom)) 
            {
                echo "<p class='msg_vert'>La vignette temporaire ".$_FILES["PME_data_vignette_medias_fichier"]["tmp_name"]." a été déplacée vers ".$vignetteDestination.$vignetteNom."</p>";
            } 
            else 
            {
                echo "<p class='msg_jaune'>La vignette n'a pas été uploadée car il est trop gros.</p>";
            }
        }
        
        // On affecte le nom de la vignette
        $newvals['vignette_medias'] = $vignetteNom;
    }
?>