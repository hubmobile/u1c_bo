<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'pathologies';
    // Champ clé unique
    $opts['key'] = 'id_pathologies';
    // Tri par défaut
    $opts['sort_field'] = array('id_pathologies');
    // Boutons navigation
    $opts['buttons']['L']['up'] = array('add');

    // Champs de la table
    $opts['fdd']['id_pathologies'] = array(
        'name'     => 'ID',
        'select'   => 'T',
        'options'  => 'LFVCPDR',
        'maxlen'   => 11,
        'default'  => '0',
        'sort'     => true
    );
    $opts['fdd']['id_pathologies']['css'] = array('postfix' => 'ColId');

    $opts['fdd']['statut_pathologies'] = array(
        'name'     => 'Statut de la fiche',
        'select'   => 'D',
        'sort'     => true
    );
    $opts['fdd']['statut_pathologies']['values2'] = array(
        '1' => 'Fiche Validée',
        '3' => 'Fiche revue par PP',
        '4' => 'Fiche revue par AD',
        '2' => 'Fiche à relire',
        '0' => 'Fiche en cours'
    );

    $opts['fdd']['titre_pathologies'] = array(
        'name'     => 'Titre de la fiche',
        'select'   => 'T',
        'options'  => 'AVCPDLF',
        'maxlen'   => 255,
        'sort'     => true
    );

    $opts['fdd']['cible_pathologies'] = array(
        'name'     => 'Cible',
        'select'   => 'D',
        'sort'     => true
    );
    $opts['fdd']['cible_pathologies']['values2'] = array(
        '1' => 'Adulte',
        '2' => 'Enfant',
        '3' => 'Adulte & Enfant'
    );

    $opts['fdd']['specialite_pathologies'] = array(
        'name'     => 'Spécialité',
        'select'   => 'M',
        'sort'     => true
    );
    $opts['fdd']['specialite_pathologies']['values2'] = array(
        '2' => 'cardiologie',
        '3' => 'dermatologie',
        '4' => 'endocrinologie',
        '5' => 'gastro-entérologie',
        '7' => 'génito-urinaire',
        '6' => 'gynécologie',
        '8' => 'hématologie',
        '1' => 'infectieux',
        '9' => 'métabolisme',
        '10' => 'neurologie',
        '11' => 'ophtalmologie',
        '12' => 'orl',
        '13' => 'pathologies circonstancielles',
        '16' => 'pédiatrie',
        '14' => 'pneumologie',
        '15' => 'psychiatrie',
        '17' => 'rhumatologie',
        '18' => 'stomatologie',
        '19' => 'symptômes',
        '20' => 'système immunitaire',
        '21' => 'toxicologie',
        '22' => 'traumatologie',
        '23' => 'vasculaire'
    );

    $opts['fdd']['pi_pathologies'] = array(
        'name'     => 'Points importants',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );

    $opts['fdd']['pi_pathologies']['escape'] = false;

    $opts['fdd']['c_pathologies'] = array(
        'name'     => 'Présentation clinique / CIMU',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['c_pathologies']['escape'] = false;

    $opts['fdd']['sp_pathologies'] = array(
        'name'     => 'Signes paracliniques',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['sp_pathologies']['escape'] = false;

    $opts['fdd']['de_pathologies'] = array(
        'name'     => 'Diagnostic étiologique',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['de_pathologies']['escape'] = false;

    $opts['fdd']['dd_pathologies'] = array(
        'name'     => 'Diagnostic différentiel',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['dd_pathologies']['escape'] = false;

    $opts['fdd']['t_pathologies'] = array(
        'name'     => 'Traitement',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['t_pathologies']['escape'] = false;

    $opts['fdd']['s_pathologies'] = array(
        'name'     => 'Surveillance',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['s_pathologies']['escape'] = false;

    $opts['fdd']['d_pathologies'] = array(
        'name'     => 'Devenir / orientation',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['d_pathologies']['escape'] = false;

    $opts['fdd']['md_pathologies'] = array(
        'name'     => 'Mécanisme / description',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['md_pathologies']['escape'] = false;

    $opts['fdd']['a_pathologies'] = array(
        'name'     => 'Algorithme',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['a_pathologies']['escape'] = false;

    $opts['fdd']['b_pathologies'] = array(
        'name'     => 'Bibliographie',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['b_pathologies']['escape'] = false;

    $opts['fdd']['auteur_pathologies'] = array(
        'name'     => 'Auteur',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 255
    );

    $opts['fdd']['r_pathologies'] = array(
        'name'     => 'Tags',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 120)
    );

    $opts['fdd']['liens_pathologies'] = array(
        'name'     => 'Liens',
        'select'   => 'T',
        'options'  => 'AVCPD',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 50)
    );
    $opts['fdd']['liens_pathologies']['escape'] = false;

    $opts['fdd']['dataset'] = array(
        'name'     => 'Dataset',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true,
        'default' => 1
    );

    // On selectionne tous les datasets
    $items = $ezdb->get_results('SELECT * FROM datasets');

    $datasetsAll = array();

    foreach($items as $item) 
    { 
        $datasetsAll[$item->version] = $item->version;
    }

    $opts['fdd']['dataset']['values2'] = $datasetsAll;

    $opts['fdd']['action'] = array(
        'name'     => 'Action',
        'select'   => 'D',
        'sort'     => true
    );

    $opts['fdd']['action']['values2'] = array(
        'ADD' => 'Ajouter',
        'DEL' => 'Supprimer',
        'UP' => 'Modifier'
    ); 
?>

<script type="text/javascript">
    $(function()
    {
        var config = {
            toolbar:   'ToolbarUrgences'
        };

        // Initialize the editor.
        // Callback function can be passed and executed after full instance creation.
        $("textarea[name='PME_data_pi_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_c_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_sp_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_de_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_dd_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_t_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_s_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_d_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_md_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_a_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_b_pathologies']").ckeditor(config);
        $("textarea[name='PME_data_liens_pathologies']").ckeditor(config);
    });
</script>


<div id="middle">
    <div id="left-column">
        <h3>Pathologies</h3>
        <ul class="nav">
            <li>Permet de saisir les fiches Pathologies</li>
            <li><b>Insertion Média :</b>
                <br />Pour insérer le Média ID 32, saisir #MEDIA_32# dans le texte du paragraphe</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Pathologies</h1>
        </div>
        <div class="table">
            <?php
            // call to phpMyEdit
            require_once 'phpMyEdit.class.php';
            new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>