<?php
if(!isset($_GET['id'])){

}else{
include("includes/config.php");
include("includes/fonctions.php");
include("includes/ezsql.php");

//on récupère les informations de la fiche
//$idUrl = substr($_GET['id','0', '31');
$idFiche = $_GET['id'];
$fiche = $ezdb->get_row("SELECT * FROM techniques WHERE id_techniques='".$idFiche."'");

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/view.css" rel="stylesheet" type="text/css" />
<title><?php echo $fiche->titre_techniques; ?></title></head>
<body>
<div id="content">
<div class="entete"><b><?php echo id2champ('cibles','label_cibles','id_cibles',$fiche->cible_techniques); ?></b>
<br />
Sp&eacute;cialit&eacute; : <?php
$specialites = explode(",", $fiche->specialite_techniques);
foreach($specialites as $specialite){
	echo id2champ('specialites','label_specialites','id_specialites',$specialite)." / ";
}

?>
</div>
<div class="contenu">

<?php if($fiche->pi_techniques!=""){
echo '<h1><a name="paragPI"></a>Points importants</h1>
'.$fiche->pi_techniques;
} ?>

<?php if($fiche->i_techniques!=""){
echo '<h1><a name="paragI"></a>Indications</h1>
'.$fiche->i_techniques;
} ?>

<?php if($fiche->ci_techniques!=""){
echo '<h1><a name="paragCI"></a>Contre-indications</h1>
'.$fiche->ci_techniques;
} ?>

<?php if($fiche->pm_techniques!=""){
echo '<h1><a name="paragPM"></a>Présentation du matériel</h1>
'.$fiche->pm_techniques;
} ?>

<?php if($fiche->dt_techniques!=""){
echo '<h1><a name="paragDT"></a>Description de la technique</h1>
'.$fiche->dt_techniques;
} ?>

<?php if($fiche->pe_techniques!=""){
echo '<h1><a name="paragPE"></a>Précautions d’emploi</h1>
'.$fiche->pe_techniques;
} ?>

<?php if($fiche->p_techniques!=""){
echo '<h1><a name="paragP"></a>Pièges éventuels</h1>
'.$fiche->p_techniques;
} ?>

<?php if($fiche->c_techniques!=""){
echo '<h1><a name="paragC"></a>Complications</h1>
'.$fiche->c_techniques;
} ?>

<?php if($fiche->s_techniques!=""){
echo '<h1><a name="paragS"></a>Surveillance</h1>
'.$fiche->s_techniques;
} ?>

<?php if($fiche->b_techniques!=""){
echo '<h1><a name="paragB"></a>Bibliographie</h1>
'.$fiche->b_techniques;
} ?>

<p style="font-weight: bold;">Auteur : <?php echo $fiche->auteur_techniques;?></p>
</div>
</div>
</body></html>