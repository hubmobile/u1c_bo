<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'medias';
    // Champ clé unique
    $opts['key'] = 'id_medias';
    // Tri par défaut
    $opts['sort_field'] = array('id_medias');
    // Boutons navigation
    $opts['buttons']['L']['up'] = array('add');
    // Triggers
    $opts['triggers']['insert']['before']  = 'medias_IB.php';
    $opts['triggers']['update']['before']  = 'medias_IB.php';

    // Champs de la table
    $opts['fdd']['id_medias'] = array(
        'name'     => 'ID',
        'select'   => 'T',
        'options'  => 'LFVCPDR', // auto increment
        'maxlen'   => 11,
        'default'  => '0',
        'sort'     => true
    );
    $opts['fdd']['id_medias']['css'] = array('postfix' => 'ColId');

    $opts['fdd']['specialite_medias'] = array(
        'name'     => 'Spécialité',
        'select'   => 'M',
        'options'  => 'ACPDV',
        'sort'     => true
    );
    $opts['fdd']['specialite_medias']['values2'] = array(
        '2' => 'cardiologie',
        '3' => 'dermatologie',
        '4' => 'endocrinologie',
        '5' => 'gastro-entérologie',
        '7' => 'génito-urinaire',
        '6' => 'gynécologie',
        '8' => 'hématologie',
        '1' => 'infectieux',
        '9' => 'métabolisme',
        '10' => 'neurologie',
        '11' => 'ophtalmologie',
        '12' => 'orl',
        '13' => 'pathologies circonstancielles',
        '16' => 'pédiatrie',
        '14' => 'pneumologie',
        '15' => 'psychiatrie',
        '17' => 'rhumatologie',
        '18' => 'stomatologie',
        '19' => 'symptômes',
        '20' => 'système immunitaire',
        '21' => 'toxicologie',
        '22' => 'traumatologie',
        '23' => 'vasculaire'
    );

    $opts['fdd']['pathologie_medias'] = array(
        'name'     => 'Fiche Patho.',
        'select'   => 'M',
        'options'  => 'ACPDV'
    );

    // On construit le tableau des valeurs
    $result = mysqli_query($ezdb->ezdbh,"SELECT id_pathologies AS id, titre_pathologies AS name FROM pathologies ORDER BY id_pathologies ASC");
    $opts['fdd']['pathologie_medias']['values2'] = array();
    $opts['fdd']['pathologie_medias']['values2'][''] = ' - aucune - ';
    
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $opts['fdd']['pathologie_medias']['values2'][$row["id"]] = $row["id"]." - ".$row["name"];
    }

    $opts['fdd']['technique_medias'] = array(
        'name'     => 'Fiche Tech.',
        'select'   => 'M',
        'options'  => 'ACPDV'
    );

    // On construit le tableau des valeurs
    $result = mysqli_query($ezdb->ezdbh, "SELECT id_techniques AS id, titre_techniques AS name FROM techniques ORDER BY id_techniques ASC");
    $opts['fdd']['technique_medias']['values2'] = array();
    $opts['fdd']['technique_medias']['values2'][''] = ' - aucune - ';
    
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $opts['fdd']['technique_medias']['values2'][$row["id"]] = $row["id"]." - ".$row["name"];
    }

$opts['fdd']['memo_medias'] = array(
    'name'     => 'Fiche IDE',
    'select'   => 'M',
    'options'  => 'ACPDV'
);

// On construit le tableau des valeurs
$result = mysqli_query($ezdb->ezdbh,"SELECT id_fiche_ide AS id, titre_fiche_ide AS name FROM fiches_ide ORDER BY id_fiche_ide ASC");
$opts['fdd']['memo_medias']['values2'] = array();
$opts['fdd']['memo_medias']['values2'][''] = ' - aucune - ';

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
    $opts['fdd']['memo_medias']['values2'][$row["id"]] = $row["id"]." - ".$row["name"];
}

    $opts['fdd']['ordre_medias'] = array(
        'name'     => 'Ordre',
        'select'   => 'D',
        'options'  => 'ACPDV',
        'sort'     => true
    );
    $opts['fdd']['ordre_medias']['values2'] = array(
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25'

    );

    $opts['fdd']['nom_medias'] = array(
        'name'     => 'Titre',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true
    );

    $opts['fdd']['legende_medias'] = array(
        'name'     => 'Légende',
        'select'   => 'T',
        'options'  => 'LACPDV',
        'textarea' => array(
            'rows' => 15,
            'cols' => 100),
        'maxlen'   => 255
    );

    $opts['fdd']['type_medias'] = array(
        'name'     => 'Type',
        'select'   => 'D',
        'maxlen'   => 255,
        'sort'     => true
    );

    $opts['fdd']['type_medias']['values2'] = array(
        '' => ' - sélectionner - ',
        'img' => 'Image',
        'pho' => 'Photo',
        'vid' => 'Vidéo',
        'tab' => 'Tableau',
        'alg' => 'Algorithme'
    );

    $opts['fdd']['fichier_medias'] = array(
        'name'     => 'Fichier',
        'select'   => 'T',
        'maxlen'   => 255
    );

    $opts['fdd']['vignette_medias'] = array(
        'name'     => 'Vignette',
        'select'   => 'T',
        'maxlen'   => 255
    );

    $opts['fdd']['tags_medias'] = array(
        'name'     => 'Tags',
        'select'   => 'T',
        'options'  => 'ACPDV',
        'maxlen'   => 65535,
        'textarea' => array(
            'rows' => 5,
            'cols' => 120)
    );

    $opts['fdd']['dataset'] = array(
        'name'     => 'Dataset',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true,
        'default' => 1
    );

    // On selectionne tous les datasets
    $items = $ezdb->get_results('SELECT * FROM datasets');

    $datasetsAll = array();

    foreach($items as $item) 
    { 
        $datasetsAll[$item->version] = $item->version;
    }

    $opts['fdd']['dataset']['values2'] = $datasetsAll;

    $opts['fdd']['action'] = array(
        'name'     => 'Action',
        'select'   => 'D',
        'sort'     => true
    );

    $opts['fdd']['action']['values2'] = array(
        'ADD' => 'Ajouter',
        'DEL' => 'Supprimer',
        'UP' => 'Modifier'
    ); 
?>
<script type="text/javascript">
    
    $(document).ready(function() {
        $("input[name='PME_data_fichier_medias']").replaceWith("<input type='file' name='PME_data_fichier_medias_fichier' /><br /><b>Fichier existant en base :</b>  " + $("input[name='PME_data_fichier_medias']").val() + "</div><input type='hidden' name='PME_data_fichier_medias' value='" + $("input[name='PME_data_fichier_medias']").val() + "' />");
        
        $("input[name='PME_data_vignette_medias']").replaceWith("<input type='file' name='PME_data_vignette_medias_fichier' /><br /><b>Vignette existante en base :</b>  " + $("input[name='PME_data_vignette_medias']").val() + "</div><input type='hidden' name='PME_data_vignette_medias' value='" + $("input[name='PME_data_vignette_medias']").val() + "' />");
    });
    
</script>

<div id="middle">
    <div id="left-column">
        <h3>Médias</h3>
        <ul class="nav">
            <li><i>Permet de déposer les fichiers médias et leur vignette</i></li>
            <li><b>Format Vidéos :</b><br />L'iphone accepte uniquement des vidéos au format .mov et .mp4</li>
            <li><b>Taille Vignettes :</b><br />Les vignettes doivent faire 93px x 68px</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Médias</h1>
        </div>
        
        <?php
            if (isset($_GET["PME_sys_operation"]) && $_GET["PME_sys_operation"] == "PME_op_View")
            {
                echo "<table style='margin:auto;'>";
                echo "<tr><th><h2>Fichier : </h2></th><th><h2>Vignette : </h2></th></tr>";
                
                $mediaFind = $ezdb->get_results('SELECT * FROM medias WHERE id_medias = '.$_GET["PME_sys_rec"].'');
                
                $extension = explode(".", $mediaFind[0]->fichier_medias);
                
                echo "<tr><td>";
                
                if ($extension[1] == "mp4")
                {
                    echo "<video controls><source src='medias/fichiers/".$mediaFind[0]->fichier_medias."' type='video/mp4'></video>";
                }
                else
                {
                    echo "<img style='max-width: 100%;' src='medias/fichiers/".$mediaFind[0]->fichier_medias."'>";
                }
                
                echo "</td><td>";
                
                echo "<img style='max-width: 100%;' src='medias/vignettes/".$mediaFind[0]->vignette_medias."'>";
                echo "</td></tr></table>";
            }
        ?>
        
        <div class="table">
            <?php
                // call to phpMyEdit
                require_once 'phpMyEdit.class.php';
                new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>