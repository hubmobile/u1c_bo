<?php
    include("includes/config.php");
    include("includes/header.php");
    include("includes/ezsql.php");

    // Table
    $opts['tb'] = 'fiches_ide';
    // Champ clé unique
    $opts['key'] = 'id_fiche_ide';
    // Tri par défaut
    $opts['sort_field'] = array('id_fiche_ide');
    // Boutons navigation
    $opts['buttons']['L']['up'] = array('add');

    // Champs de la table
    $opts['fdd']['id_fiche_ide'] = array(
      'name'     => 'ID',
      'select'   => 'T',
      'options'  => 'LFVCPDR',
      'maxlen'   => 11,
      'default'  => '0',
      'sort'     => true
    );
    $opts['fdd']['id_fiche_ide']['css'] = array('postfix' => 'ColId');

    $opts['fdd']['statut_fiche_ide'] = array(
      'name'     => 'Statut de la fiche',
      'select'   => 'D',
      'sort'     => true
    );
    $opts['fdd']['statut_fiche_ide']['values2'] = array(
        '1' => 'Fiche Validée',
        '3' => 'Fiche revue par PP',
        '4' => 'Fiche revue par AD',
        '2' => 'Fiche à relire',
        '0' => 'Fiche en cours'
    );

    $opts['fdd']['titre_fiche_ide'] = array(
      'name'     => 'Titre de la fiche',
      'select'   => 'T',
      'options'  => 'AVCPDLF',
      'maxlen'   => 255,
      'sort'     => true
    );

    $opts['fdd']['contenu_fiche_ide'] = array(
      'name'     => 'Contenu',
      'select'   => 'T',
      'options'  => 'AVCPD',
      'maxlen'   => 65535,
      'textarea' => array(
        'rows' => 5,
        'cols' => 50)
    );
    $opts['fdd']['contenu_fiche_ide']['escape'] = false;

    $opts['fdd']['dataset'] = array(
        'name'     => 'Dataset',
        'select'   => 'T',
        'maxlen'   => 255,
        'sort'     => true,
        'default' => 1
    );

    // On selectionne tous les datasets
    $items = $ezdb->get_results('SELECT * FROM datasets');

    $datasetsAll = array();

    foreach($items as $item) 
    { 
        $datasetsAll[$item->version] = $item->version;
    }

    $opts['fdd']['dataset']['values2'] = $datasetsAll;

    $opts['fdd']['action'] = array(
        'name'     => 'Action',
        'select'   => 'D',
        'sort'     => true
    );

    $opts['fdd']['action']['values2'] = array(
        'ADD' => 'Ajouter',
        'DEL' => 'Supprimer',
        'UP' => 'Modifier'
    ); 
?>

<script type="text/javascript">
    $(function() {
        var config = {
            toolbar:   'ToolbarUrgences'
        };

        // Initialize the editor.
        // Callback function can be passed and executed after full instance creation.
        $("textarea[name='PME_data_contenu_fiche_ide']").ckeditor(config);
    });
</script>

<div id="middle">
    <div id="left-column">
        <h3>Fiches IDE</h3>
        <ul class="nav">
            <li>Permet de saisir les fiches IDE</li>
            <li><b>Insertion TABLEAU IMAGES :</b><br />Ajouter la class "swipe" aux tableaux avec image</li>
        </ul>
    </div>
    <div id="center-column">
        <div class="top-bar">
            <h1>Fiches IDE</h1>
        </div>
        <div class="table">
            <?php
                // Call to phpMyEdit
                require_once 'phpMyEdit.class.php';
                new phpMyEdit($opts);
            ?>
        </div>
    </div>
</div>

<?php
    include("includes/footer.php");
?>